<!--A Design by Techlabz Infosoft Pvt Ltd
  Author: Techlabz Infosoft Pvt.Ltd
  Author URL: http://www.techlabzinfosoft.com
 
  -->
<!DOCTYPE html>
<html lang="zxx">
  <head>
    <title>The Bsel| Home </title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <script>
      addEventListener("load", function () {
      	setTimeout(hideURLbar, 0);
      }, false);
      
      function hideURLbar() {
      	window.scrollTo(0, 1);
      }
    </script>
    <!--//meta tags ends here-->
    <!--booststrap-->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- //font-awesome icons -->
    <link href="css/blast.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style10.css" />
    <!--stylesheets-->
    <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Merriweather:300,400,700,900" rel="stylesheet">
  </head>
  <body>
    <div class="blast-box">
      <div class="blast-icon"><span class="fas fa-tint"></span></div>
      <div class="blast-frame">
        <p>change colors</p>
        <div class="blast-colors">
          <div class="blast-color">#86bc42</div>
          <div class="blast-color">#8373ce</div>
          <div class="blast-color">#14d4f4</div>
          <div class="blast-color">#72284b</div>
        </div>
        <p class="blast-custom-colors">Custom colors</p>
        <input type="color" name="blastCustomColor" value="#cf2626">
      </div>
    </div>
    
    <div class="header-outs" id="header">
      <div class="header-w3layouts">
        <div class="container">
          <div class="row headder-contact">
            <div class="col-lg-6 col-md-7 col-sm-9 info-contact-agile">
              <ul>
                <li>
                  <span class="fas fa-phone-volume" ></span>
                  <p>+91 98354 29138</p>
                </li>
                <li>
                  <span class="fas fa-envelope"></span>
                  <p><a href="mailto:info@example.com">info@thebsel.in</a></p>
                </li>
                <li>
                <span class="fas fa-user"></span>
                    <p>  <a href="login.php"  >Student login</a></p>
                   
                </li>
              </ul>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-3 icons px-0">
              <ul>
                <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                
                <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                
              </ul>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="hedder-up">
            <h1 ><a href="index.php" class="navbar-brand" data-blast="color">Thebsel</a></h1>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="headdser-nav-color" data-blast="bgColor">
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
              <ul class="navbar-nav ">
                <li class="nav-item active">
                  <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                
                <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Why Us
                                
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="mission-vission.php">Mission/Vission</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="massage-director.php">Director Massege</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="about.php">About</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="Franchies.php">Franchisee</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="branch.php">Branches</a>


                            </div>
                        </li>

                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Our Cources
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="dca.php">DCA</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="dtp.php">DTP</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="adca.php">ADCA</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="tally.php">TALLY</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="c.php">C Language</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="php.php">PHP Language</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="java.php">JAVA</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="oracle.php">ORACLE</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="Webdesign.php">Webdesign</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="english.php">English</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="computer-teacher-training-course.php">Computer Training</a>

                            </div>
                        </li>
                 <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Academics
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="top-faculties.php">Top Faculties</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="batch.php">Batch Schedules</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="result.php">Result</a>

                            </div>
                        </li>
                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Gallery
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="gallery-photo.php">Photo Gallery</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="gallery-video.php">Video Gallery</a>

                            </div>
                        </li>
                        
                
                <li class="nav-item">
                  <a href="contact.php" >Contact</a>
                </li>
                 
              </ul>
            </div>
          </div>
        </nav>
        <!--//navigation section -->
        <div class="clearfix"> </div>
      </div><!-- banner -->
      
  <img src="images/cc6.jpg" class="img-responsive">

<!-- //banner -->
<!-- banner1 -->
	

	<div class="services-breadcrumb">
		<div class="container">
			<p><a href="index.php"  data-toggle="modal" class="info" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Home</a>|About</p>
		</div>
	</div>
	
<!-- //banner1 -->

<!--Team-->
    <section class="team py-lg-4 py-md-3 py-sm-3 py-3" id="team">
      <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
        <h3 class="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">Our Staff </h3>
        <div class="row ">
          <div class="col-lg-3 col-md-6 col-sm-6 profile">
            <div class="team-shadow">
              <div class="img-box">
                <img src="images/t1.jpg" alt="">
                <div class="list-social-icons">
                  <ul>
                    <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                    <li><a href="#"><span class="fas fa-envelope"></span></a></li>
                    <li><a href="#"><span class="fas fa-rss"></span></a></li>
                    <li><a href="#"><span class="fab fa-vk"></span></a></li>
                  </ul>
                </div>
              </div>
              <div class="team-w3layouts-info py-lg-4 py-3 text-center" data-blast="bgColor">
                <h4 class="text-white mb-2">Rox Will</h4>
                <span class="wls-client-title text-black">Professor</span>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 profile">
            <div class="team-shadow">
              <div class="img-box">
                <img src="images/t2.jpg" alt="">
                <div class="list-social-icons">
                  <ul>
                    <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                    <li><a href="#"><span class="fas fa-envelope"></span></a></li>
                    <li><a href="#"><span class="fas fa-rss"></span></a></li>
                    <li><a href="#"><span class="fab fa-vk"></span></a></li>
                  </ul>
                </div>
              </div>
              <div class="team-w3layouts-info py-lg-4 py-3 text-center" data-blast="bgColor">
                <h4 class="text-white mb-2">Sam Doi</h4>
                <span class="wls-client-title text-black">Professor</span>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 profile">
            <div class="team-shadow">
              <div class="img-box">
                <div class="team-list-img">
                  <img src="images/t3.jpg" alt="">
                </div>
                <div class="list-social-icons">
                  <ul>
                    <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                    <li><a href="#"><span class="fas fa-envelope"></span></a></li>
                    <li><a href="#"><span class="fas fa-rss"></span></a></li>
                    <li><a href="#"><span class="fab fa-vk"></span></a></li>
                  </ul>
                </div>
              </div>
              <div class="team-w3layouts-info py-lg-4 py-3 text-center" data-blast="bgColor">
                <h4 class="text-white mb-2">Kent Kelly</h4>
                <span class="wls-client-title text-black">Professor</span>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 profile">
            <div class="team-shadow">
              <div class="img-box">
                <div class="team-list-img">
                  <img src="images/t4.jpg" alt="">
                </div>
                <div class="list-social-icons">
                  <ul>
                    <li><a href="#"><span class="fab fa-facebook-f" ></span></a></li>
                    <li><a href="#"><span class="fas fa-envelope"></span></a></li>
                    <li><a href="#"><span class="fas fa-rss"></span></a></li>
                    <li><a href="#"><span class="fab fa-vk"></span></a></li>
                  </ul>
                </div>
              </div>
              <div class="team-w3layouts-info py-lg-4 py-3 text-center" data-blast="bgColor">
                <h4 class="text-white mb-2">Kent Kelly</h4>
                <span class="wls-client-title text-black">Professor</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--//Team-->
     <!--footer-->
    <?php include 'footer.php'  ?>