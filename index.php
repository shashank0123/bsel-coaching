
<html lang="zxx">
  <head>
    <title>The Bsel || Home </title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <script>
      addEventListener("load", function () {
      	setTimeout(hideURLbar, 0);
      }, false);
      
      function hideURLbar() {
      	window.scrollTo(0, 1);
      }
    </script>
    <!--//meta tags ends here-->
    <!--booststrap-->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- //font-awesome icons -->
    <link href="css/blast.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style10.css" />
    <!--stylesheets-->
    <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
    <link href="css/swc.css" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Merriweather:300,400,700,900" rel="stylesheet">
    <script type="text/javascript" src="js/swc.js"></script>
   
  </head>
  <body>

    <div class="blast-box">
      <div class="blast-icon"><span class="fas fa-tint"></span></div>
      <div class="blast-frame">
        <p>change colors</p>
        <div class="blast-colors">
          <div class="blast-color">#86bc42</div>
          <div class="blast-color">#8373ce</div>
          <div class="blast-color">#14d4f4</div>
          <div class="blast-color">#72284b</div>
        </div>
        <p class="blast-custom-colors">Custom colors</p>
        <input type="color" name="blastCustomColor" value="#cf2626">
      </div>
    </div>
    
    <div class="header-outs" id="header">
      <div class="header-w3layouts">
        <div class="container">
          <div class="row headder-contact">
            <div class="col-lg-6 col-md-7 col-sm-9 info-contact-agile">
              <ul>
                <li>
                  <span class="fas fa-phone-volume" ></span>
                  <p>+91 98354 29138</p>
                </li>
                <li>
                  <span class="fas fa-envelope"></span>
                  <p><a href="mailto:info@example.com">info@thebsel.in</a></p>
                </li>
                <li>
                
                      <a href="login.php" >Student login</a>
                   
                </li>
              </ul>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-3 icons px-0">
              <ul>
                <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                
                <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                
              </ul>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="hedder-up">
            <h1 ><a href="index.php" class="navbar-brand" data-blast="color"><img src="images/logobsel.jpeg" class="img-responsive" width="200px;"></a></h1>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="headdser-nav-color" data-blast="bgColor">
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
              <ul class="navbar-nav ">
                <li class="nav-item active">
                  <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                
                <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Why Us
                                
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="mission-vission.php">Mission/Vission</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="massage-director.php">Director Massege</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="about.php">About</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="Franchies.php">Franchisee</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="branch.php">Branches</a>


                            </div>
                        </li>
                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Our Cources
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="dca.php">DCA</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="dtp.php">ADIT</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="adca.php">ADCA</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="tally.php">TALLY</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="c.php">C Language</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="php.php">PHP Language</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="java.php">JAVA</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="oracle.php">ORACLE</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="Webdesign.php">Webdesign</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="english.php">English</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="computer-teacher-training-course.php">Computer Training</a>

                            </div>
                        </li>
                 <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Academics
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="top-faculties.php">Top Faculties</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="batch.php">Batch Schedules</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="result.php">Result</a>

                            </div>
                        </li>
                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Gallery
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="gallery-photo.php">Photo Gallery</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="gallery-video.php">Video Gallery</a>

                            </div>
                        </li>
                        
                
                <li class="nav-item">
                  <a href="contact.php" >Contact</a>
                </li>
                 
              </ul>
            </div>
          </div>
        </nav>
        <!--//navigation section -->
        <div class="clearfix"> </div>
      </div>
      <!--banner -->
      <!-- Slideshow 4 -->
      <div class="slider">
        <div class="callbacks_container">
          <ul class="rslides" id="slider4">
            <li>
              <div class="slider-img one-img">
                <div class="container">
                  <div class="slider-info text-left">
                    <h4 >English Spoken Institute</h4>
                    <h5>The Best English Spoken Institute in Muzaffarpur</h5>
                   
                    
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="slider-img two-img">
                <div class="container">
                  <div class="slider-info text-left">
                    <h4>Study</h4>
                    <h5>Computer Traninig Institute In Muzaffarpur</h5>
                   
                    
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="slider-img three-img">
                <div class="container">
                  <div class="slider-info text-left">
                    <h4>The Best Institute of Language</h4>
                    <h5>DCA/PHP/WEB DESIGN Training in Muzaffarpur</h5>
                    
                    
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <!-- This is here just to demonstrate the callbacks -->
        <!-- <ul class="events">
          <li>Example 4 callback events</li>
          </ul>-->
        <div class="clearfix"></div>
      </div>
    </div>
    <!-- //banner -->
    <!-- about -->
  <div class="about py-5 pt-sm-4 pt-3">
    <div class="container py-xl-5 py-lg-3">
      <h3 class="title text-capitalize font-weight-light text-dark text-center mb-5 ">Welcome to
        <span class="font-weight-bold">The Bsel</span>
      </h3>
      <div class="row pt-md-4">
        <div class="col-lg-6 about_right">
          <h3 class="text-capitalize text-right font-weight-light font-italic">interface friendly learning at
            <span class="font-weight-bold">The Bsel</span>
          </h3>
          <p class="text-right my-4 pr-4 border-right">The BSEL , English training academy with computer education and call center training institute has been one of the most reputed institutions of india. it was established in 1990 with a view to making people speak and write english with fluency, rhythm and style. 

</p>
        
        </div>
        <div class="col-lg-6 left-img-agikes mt-lg-0 mt-sm-4 mt-3 text-right">
          <h3 class="title text-capitalize font-weight-light text-dark text-center mb-5">Notice
            <span class="font-weight-bold">Board</span>
          </h3>
          
          
          
            <div class="categories my-4 p-4 border">
              
              <marquee direction="up" scrollamount=2>
              
                
               <p>Welcome To The BSEL</p>
               <p>New Batch Coming Soon</p>
                </marquee>
                <!--<tr><td style=' padding:5px;'><b><a href='subcat.php?catid=$data[0]'>$data[1]</a></b></td></tr>-->
                  
            </div>
          
        </div>
      </div>
    </div>
  </div>
  <!-- //about -->
    <!--about-->
    <section class="pt-md-5 pt-sm-4 pt-3">
      <div class="container-fluid ">
        <div class="main row ">
          <!-- TENTH EXAMPLE -->
          <div class="col-lg-4 view view-tenth">
            <img src="images/ab30.jpg" alt="" class="img-fluid">
            <div class="mask">
              <h3 data-blast="bgColor">Success</h3>
             
              <a href="#" class="info" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
            </div>
          </div>
          <div class="col-lg-4 view view-tenth">
            <img src="images/ab31.jpg" alt="" class="img-fluid">
            <div class="mask">
              <h3 data-blast="bgColor">Hard Work</h3>
             
              <a href="#" class="info" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
            </div>
          </div>
          <div class="col-lg-4 view view-tenth">
            <img src="images/ab32.jpg" alt="" class="img-fluid">
            <div class="mask">
              <h3 data-blast="bgColor">Admission</h3>
             
              <a href="admission.php" class="info"   data-blast="bgColor">Read More</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="about" id="about">
      <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
        <!--Horizontal Tab-->
        <div id="horizontalTab">
          <ul class="resp-tabs-list justify-content-center">
            <li data-blast="bgColor">Mission</li>
            <li data-blast="bgColor">Vission</li>
            <li data-blast="bgColor">franchisee </li>
            <li data-blast="bgColor">Lingua lab</li>
          </ul>
          <div class="resp-tabs-container">
            <div class="tab1" >
              <div class="row mt-lg-4 mt-3">
                <div class="col-md-7 latest-list">
                  <div class="about-jewel-agile-left">
                    <h4 class="mb-3" data-blast="color">Mission </h4>
                    <p>To create better speakers and better leaders by providing opportunities, to excel in communication skill as well as computer skill by teaching them in the most effective way</p>
                    <h5 data-blast="color"> What our Mission</h5>
                  </div>
                </div>
                <div class="col-md-5 about-txt-img">
                  <img src="images/ab20.jpg" class="img-thumbnail" alt="">
                </div>
              </div>
            </div>
            <div class="tab2">
              <div class="row mt-lg-4 mt-3">
                <div class="col-md-5 about-txt-img">
                  <img src="images/ab21.jpg" class="img-thumbnail" alt="">
                </div>
                <div class="col-md-7 latest-list">
                  <div class="about-jewel-agile-left">
                    <h4 class="mb-3" data-blast="color"> Vission</h4>
                    <p>To spread quality training in spoken english as well as in computer education to all those people who need these skills in moulding their future through out india.</p>
                    <h5 data-blast="color">What our Vission</h5>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab3">
              <div class="row mt-lg-4 mt-3">
                <div class="col-md-7 latest-list">
                  <div class="about-jewel-agile-left">
                    <h4 class="mb-3" data-blast="color">franchisee</h4>
                    <p>we are expanding our business and hence we are looking for dynemic business partners we feel in this era of globalization, English and computer knowledge have become a demanding skill to excel in the field of IT , hospitility, tourism exports, BPO and medical . </p>
                    <h5 data-blast="color">Our franchisee</h5>
                  </div>
                </div>
                <div class="col-md-5 about-txt-img">
                  <img src="images/ab23.jpg" class="img-thumbnail" alt="">
                </div>
              </div>
            </div>
            <div class="tab4">
              <div class="row mt-lg-4 mt-3">
                <div class="col-md-5 about-txt-img">
                  <img src="images/ab22.jpg" class="img-thumbnail" alt="">
                </div>
                <div class="col-md-7 latest-list">
                  <div class="about-jewel-agile-left">
                    <h4 class="mb-3" data-blast="color">Lingua lab</h4>
                    <p>Remember "Practice makes a man perfect" fluency in a language is the result of constant  practice. without practice , you won"t be able to speak english fluently. in order to provide such continuous practice this institute holds a special "language practice session" called the "lingua-lab".</p>
                    <h5 data-blast="color">What Our Lingua lab</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--//about-->
   
    <!--blog -->
    <section class="blog py-lg-4 py-md-3 py-sm-3 py-3" id="blog">
      <div class="container py-lg-5 py-md-4 py-sm-4 py-3">
        <h3 class="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">Our Cources </h3>
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-6 blog-grid-flex">
            <div class="clients-color">
              <img src="images/ab6.jpg" class="img-fluid" alt="">
              <div class="blog-txt-info">
                <h4 class="mt-2"><a href="dca.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">DCA</a></h4>
                
                <p>Introduction to computer,The computer Generation,Classification of Computer, Number System</p>
                <div class="outs_more-buttn" >
                  <a href="dca.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6  blog-grid-flex">
            <div class="clients-color">
              <img src="images/ab7.jpg" class="img-fluid" alt="">
              <div class="blog-txt-info">
                <h4 class="mt-2"><a href="dit.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="color"> ADIT</a></h4>
                
                <p>Advanced Diploma in Information Technology (CCA + DCA + CFA + DCP = ADIT)</p>
                <div class="outs_more-buttn" >
                  <a href="dit.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 blog-grid-flex">
            <div class="clients-color">
              <img src="images/ab8.jpg" class="img-fluid" alt="">
              <div class="blog-txt-info">
                <h4 class="mt-2"><a href="adca.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">ADCA</a></h4>
                
                <p>Advanced Diploma in Computer Application (CCA + DCA + CFA + DTP = ADCA)</p>
                <div class="outs_more-buttn" >
                  <a href="adca.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 blog-grid-flex mt-lg-5 mt-md-4 mt-sm-3 mt-3">
            <div class="clients-color">
              <img src="images/ab9.jpg" class="img-fluid" alt="">
              <div class="blog-txt-info">
                <h4 class="mt-2"><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">TALLY </a></h4>
                
                <p>Tally ERp 9.0,Investnent , TradingGST Training, Income Tax</p>
                <div class="outs_more-buttn" >
                  <a href="tally.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 blog-grid-flex mt-lg-5 mt-md-4 mt-sm-3 mt-3" >
            <div class="clients-color">
              <img src="images/ab10.jpg" class="img-fluid" alt="">
              <div class="blog-txt-info">
                <h4 class="mt-2"><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">C / C++</a></h4>
               
                <p>>The objective of the course is to enable a student to acquire the knowledge pertaining to C /C++</p>
                <div class="outs_more-buttn" >
                  <a href="c.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 blog-grid-flex mt-lg-5 mt-md-4 mt-sm-3 mt-3">
            <div class="clients-color">
              <img src="images/ab11.jpg" class="img-fluid" alt="">
              <div class="blog-txt-info">
                <h4 class="mt-2"><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">      PHP </a></h4>
               
                <p>Php Is a language where web page is created .This language is used for only scripting web page</p>
                <div class="outs_more-buttn" >
                  <a href="php.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
                </div>
              </div>
            </div>
          </div>
           <div class="col-lg-4 col-md-6 col-sm-6 blog-grid-flex mt-lg-5 mt-md-4 mt-sm-3 mt-3">
            <div class="clients-color">
              <img src="images/ab12.jpg" class="img-fluid" alt="">
              <div class="blog-txt-info">
                <h4 class="mt-2"><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color">JAVA </a></h4>
                
                <p>Java is a programing language where it use for software making .This language is more power full then other.</p>
                <div class="outs_more-buttn" >
                  <a href="java.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
                </div>
              </div>
            </div>
          </div>
             <div class="col-lg-4 col-md-6 col-sm-6 blog-grid-flex mt-lg-5 mt-md-4 mt-sm-3 mt-3">
            <div class="clients-color">
              <img src="images/ab13.jpg" class="img-fluid" alt="">
              <div class="blog-txt-info">
                <h4 class="mt-2"><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color"> English Spoken </a></h4>
               
                <p>With globalization English has become the need of the present era. English acts like a </p>
                <div class="outs_more-buttn" >
                  <a href="english.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
                </div>
              </div>
            </div>
          </div>
           <div class="col-lg-4 col-md-6 col-sm-6 blog-grid-flex mt-lg-5 mt-md-4 mt-sm-3 mt-3">
            <div class="clients-color">
              <img src="images/ab14.jpg" class="img-fluid" alt="">
              <div class="blog-txt-info">
                <h4 class="mt-2"><a href="#" data-toggle="modal" data-target="#exampleModalLive" data-blast="color"> computer training  </a></h4>
                
                <p>Computer Teacher training is for teacher where all concept teach to teacher</p>
                <div class="outs_more-buttn" >
                  <a href="computer-teacher-training-course.php" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Read More</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--//blog -->
    <!--Subscribe-->
   
    <!--//Subscribe-->
    <!--stats-->
    <section class="stats py-lg-4 py-md-3 py-sm-3 py-3" id="stats">
      <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
        <h3 class="title clr text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">Our Stats </h3>
        <div class="jst-must-info text-center">
          <div class="row stats-info">
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 stats-grid-1">
              <div class="stats-grid" data-blast="bgColor">
                <div class="counter">2045</div>
                <div class="stat-info">
                  <h5 class="pt-2">Experience </h5>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 stats-grid-2">
              <div class=" stats-grid" data-blast="bgColor">
                <div class="counter">350</div>
                <div class="stat-info">
                  <h5 class="pt-2"> Staff</h5>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 stats-grid-3">
              <div class=" stats-grid" data-blast="bgColor">
                <div class="counter">100</div>
                <div class="stat-info">
                  <h5 class="pt-2"> Courese</h5>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 stats-grid-4">
              <div class=" stats-grid" data-blast="bgColor">
                <div class="counter">1050</div>
                <div class="stat-info">
                  <h5 class="pt-2"> Students </h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--//stats-->
    
      <div id="boxes">
<div style="top: 50%; left: 50%; display: none;" id="dialog" class="window"> 
<div id="san">
<a href="#" class="close agree"><img src="images/close-icon.png" width="25" style="float:right; margin-right: -25px; margin-top: -20px;"></a>
<img src="images/san-web-corner.jpg" width="420">

</div>
</div>
<div style="width: 2478px; font-size: 32pt; color:white; height: 1202px; display: none; opacity: 0.4;" id="mask"></div>
</div>
    <!--footer-->
 
     <?php include 'footer.php'  ?>