<!--A Design by Techlabz Infosoft Pvt Ltd
  Author: Techlabz Infosoft Pvt.Ltd
  Author URL: http://www.techlabzinfosoft.com
 
  -->
<!DOCTYPE html>
<html lang="zxx">
  <head>
    <title>The Bsel| Home </title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <script>
      addEventListener("load", function () {
      	setTimeout(hideURLbar, 0);
      }, false);
      
      function hideURLbar() {
      	window.scrollTo(0, 1);
      }
    </script>
    <!--//meta tags ends here-->
    <!--booststrap-->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- //font-awesome icons -->
    <link href="css/blast.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style10.css" />
    <!--stylesheets-->
    <link href="css/main.css" rel='stylesheet' type='text/css' media="all">
    <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Merriweather:300,400,700,900" rel="stylesheet">
  </head>
  <body>
    <div class="blast-box">
      <div class="blast-icon"><span class="fas fa-tint"></span></div>
      <div class="blast-frame">
        <p>change colors</p>
        <div class="blast-colors">
          <div class="blast-color">#86bc42</div>
          <div class="blast-color">#8373ce</div>
          <div class="blast-color">#14d4f4</div>
          <div class="blast-color">#72284b</div>
        </div>
        <p class="blast-custom-colors">Custom colors</p>
        <input type="color" name="blastCustomColor" value="#cf2626">
      </div>
    </div>
    
    <div class="header-outs" id="header">
      <div class="header-w3layouts">
        <div class="container">
          <div class="row headder-contact">
            <div class="col-lg-6 col-md-7 col-sm-9 info-contact-agile">
              <ul>
                <li>
                  <span class="fas fa-phone-volume" ></span>
                  <p>+91 98354 29138</p>
                </li>
                <li>
                  <span class="fas fa-envelope"></span>
                  <p><a href="mailto:info@example.com">info@thebsel.in</a></p>
                </li>
                <li>
                <span class="fas fa-user"></span>
                    <p>  <a href="login.php"  >Student login</a></p>
                   
                </li>
              </ul>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-3 icons px-0">
              <ul>
                <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                
                <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                
              </ul>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="hedder-up">
            <h1 ><a href="index.php" class="navbar-brand" data-blast="color">Thebsel</a></h1>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="headdser-nav-color" data-blast="bgColor">
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
              <ul class="navbar-nav ">
                <li class="nav-item active">
                  <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                
                <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Why Us
                                
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="mission-vission.php">Mission/Vission</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="massage-director.php">Director Massege</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="about.php">About</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="Franchies.php">Franchisee</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="branch.php">Branches</a>


                            </div>
                        </li>

                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Our Cources
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="dca.php">DCA</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="dtp.php">DTP</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="adca.php">ADCA</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="tally.php">TALLY</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="c.php">C Language</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="php.php">PHP Language</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="java.php">JAVA</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="oracle.php">ORACLE</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="Webdesign.php">Webdesign</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="english.php">English</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="computer-teacher-training-course.php">Computer Training</a>

                            </div>
                        </li>
                 <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Academics
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="top-faculties.php">Top Faculties</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="batch.php">Batch Schedules</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="result.php">Result</a>

                            </div>
                        </li>
                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Gallery
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="gallery-photo.php">Photo Gallery</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="gallery-video.php">Video Gallery</a>

                            </div>
                        </li>
                        
                
                <li class="nav-item">
                  <a href="contact.php" >Contact</a>
                </li>
                 
              </ul>
            </div>
          </div>
        </nav>
        <!--//navigation section -->
        <div class="clearfix"> </div>
      </div><!-- banner -->
      
  <img src="images/cc6.jpg" class="img-responsive">

<!-- //banner -->
<!-- banner1 -->
	

	<div class="services-breadcrumb">
		<div class="container">
			<p><a href="index.php"  data-toggle="modal" class="info" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Home</a>|About</p>
		</div>
	</div>
	
<!-- //banner1 -->

<!--Team-->
    <section class="team py-lg-4 py-md-3 py-sm-3 py-3" id="team">
      <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
        <h3 class="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">Java </h3>
        <div class="row ">
      <!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header header-bg  text-center font-weight-bold">
        
        <h2 class="modal-title"><i class="fa fa-headphones"></i> Course Enquiry Form</h2>
                         
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
          <div class="card">
                     
                         <div class="card-body footer-bg">
                             <div class="form-group">
                                
                                 <input name="ctl00$ContentPlaceHolder1$txtenquiryname" type="text" id="ContentPlaceHolder1_txtenquiryname" class="form-control" placeholder="Enter Your Name" />
                             </div>
                             <div class="form-group">
                               
                                 <input name="ctl00$ContentPlaceHolder1$txtenquiryemail" type="email" id="ContentPlaceHolder1_txtenquiryemail" class="form-control" placeholder="Enter Your Email" />
                             </div>
                             <div class="form-group">
                               
                                 <input name="ctl00$ContentPlaceHolder1$txtenquiryphone" type="text" id="ContentPlaceHolder1_txtenquiryphone" class="form-control" placeholder="Enter Your Mobile No" />
                             </div>
                             <div class="form-group">
                               
                                 <select name="ctl00$ContentPlaceHolder1$ddlbranches" id="ContentPlaceHolder1_ddlbranches" class="custom-select">
  <option value="Any">Select Your Preferred DICS branch</option>
  <option value="1">DICS GTB Nagar,Kingsway Camp</option>
  <option value="2">DICS Pitampura</option>
  <option value="3">DICS Punjabi Bagh</option>
  <option value="4">DICS Raja Garden</option>
  <option value="5">DICS Janakpuri</option>
  <option value="6">DICS Karol Bagh</option>
  <option value="7">DICS Faridabad</option>
  <option value="8">DICS Ghaziabad</option>
  <option value="9">DICS Panipat</option>
  <option value="10">DICS Preet Vihar</option>

</select>
                             </div>
                             <div class="form-group">
                               
                                 <select name="ctl00$ContentPlaceHolder1$ddlcourses" id="ContentPlaceHolder1_ddlcourses" class="custom-select">
  <option value="Any">Select Your Preferred Course</option>
  <option value="30">Certificate in Computer Administration and Management (CCAM)</option>
  <option value="29">Management Information System (MIS)</option>
  <option value="28">Microsoft Certified System Engineer (MCSE)</option>
  <option value="27">Cisco Certified Network Associate (CCNA)</option>
  <option value="26">Master in Animation</option>
  <option value="25">Multimedia Studio</option>
  <option value="24">Advance Diploma in Financial Accounting</option>
  <option value="23">Advance Diploma in Computer Hardware and Networking (ADCHN)</option>
  <option value="22">Diploma in Computer Hardware and Networking (DCHN)</option>
  <option value="21">C Language</option>
  <option value="20">Digital Marketing</option>
  <option value="19">PHP Web Development</option>
  <option value="18">Diploma in Web Designing and Promotion</option>
  <option value="17">Programming in Python</option>
  <option value="16">Microsoft .NET</option>
  <option selected="selected" value="15">Advance Java</option>
  <option value="14">Web Studio</option>
  <option value="13">OOPS through Java (Core)</option>
  <option value="12">Android Mobile App Development</option>
  <option value="11">Master Diploma in Software Engineering (MDSE)</option>
  <option value="10">Master Diploma in Application Development (MDAD)</option>
  <option value="9">Professional Diploma in Application Development (PDAD)</option>
  <option value="8">Graphic Studio</option>
  <option value="7">Professional Diploma in Software Engineering (PDSE)</option>
  <option value="6">DOEACC A Level / Advance Diploma in Software Engineering (ADSE)</option>
  <option value="5">Certificate in Financial Accounting (CIFA)</option>
  <option value="4">Diploma in Financial Accounting (DIFA)</option>
  <option value="3">DOEACC O Level / Diploma in Computer Business Application (DCBA)</option>
  <option value="1">DOEACC CCC / Basic Computer Concept (BCC)</option>

</select>
                             </div>
                             <div class="form-group">
                               
                                 <textarea name="ctl00$ContentPlaceHolder1$txtmessage" rows="2" cols="20" id="ContentPlaceHolder1_txtmessage" class="form-control" placeholder="Write your query">
</textarea>
                             </div>
                             <small class="clearfix">By clicking on Request Callback, I allow DICS to contact , use & share my personal data as per the <a href="#">Privacy Policy.</a></small>
                            
                             <input type="submit" name="ctl00$ContentPlaceHolder1$btnsend" value="Request Callback" id="ContentPlaceHolder1_btnsend" class="btn btn-warning" />
                             
                         </div>

                        
                     
                 </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>

</div>

 <div class="container-fluid mt-1 mb-1">
          <div class="row">
              <div class="col-md-3">
                 
                  
        <ul class="list-group">
            <li class="list-group-item bg-warning text-dark font-weight-bold"> <i class="fa fa-book"></i> Our Courses</li>
    
        
                  <li class="list-group-item bold-text bg-dark"><a href='dca.php' class="text-white"><i class="fa fa-cog"></i> DCA</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='dtp.php' class="text-white"><i class="fa fa-cog"></i>DTP</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='adca.php' class="text-white"><i class="fa fa-cog"></i> ADCA</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='tally.php' class="text-white"><i class="fa fa-cog"></i>TALLY</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='c.php' class="text-white"><i class="fa fa-cog"></i> C, C++</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='java.php' class="text-white"><i class="fa fa-cog"></i> JAVA,</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='oracle.php' class="text-white"><i class="fa fa-cog"></i>, ORAcle</a></li>
                   <li class="list-group-item bold-text bg-dark"><a href='php.php' class="text-white"><i class="fa fa-cog"></i>, PHP</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='computer-teacher-training-course.php' class="text-white"><i class="fa fa-cog"></i> computer training </a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='english.php' class="text-white"><i class="fa fa-cog"></i> English</a></li>
                     
                     
                     
                 
    
         </ul>
    

                 
              </div>
               <div class="col-md-9">
                  
    

        
              <h2 class="text-danger font-weight-bold"> Advance Java</h2>
               <hr class="hr-yellow"/>
               <p class="text-justify">
                 <a href='/docs/courses/' class="btn btn-success">Download Syllabus <i class="fa fa-download"></i></a>  <a href="#" class="btn btn-warning"  data-toggle="modal" data-target="#myModal">Callback Request <i class="fa fa-envelope-open" aria-hidden="true"></i></a> 
                  <div class="clearfix"></div>
                 <strong>Duration : </strong> 6 Months  /  <strong>Type :</strong> Certificate / <strong>Mode : </strong> Part Time  
                  
                 
               </p>
              
                <p class="text-justify"><b>Objective :</b>The objective of the course is to enable a student to acquire the knowledge pertaining to advance Java.</p><p class="text-justify"><b>Eligibility/Prerequisite : </b> Knowledge of Java(Core)</p><h2 class="text-primary"> <i class="fa fa-cog"></i> Course Structure</h2> <div class="row"><div class="col-md-3"><ul type="square"><li><b class="text-danger">Swing Programming</b><ul><li>Understanding Difference between</li><li>Swing &amp; AWT Programming</li><li>Define Swing Component</li><li>List Swing Packages</li><li>List the Sub-Classes Handles Events</li><li>Use JFC to write Wing applets</li><li>Understanding the concept of  "Look &amp; Feel "</li></ul></li></ul><ul type="square"><li><b class="text-danger">Spring</b><ul><li>Introduction to Spring</li><li>Spring Modules</li><li>Spring Applications</li><li>Spring JDBC</li><li>Spring Web</li></ul></li></ul></div><div class="col-md-3"><ul type="square"><li><b class="text-danger">Java Database Connectivity</b><ul><li>Define JDBC API</li><li>Describe the various JDBC Drivers</li><li>Identify JDBC products</li><li>Outline JDBC design consideration</li><li>Describe the 2-tier Server-Client model</li><li>Use JDBC to access a database</li><li>Setup connection to a database</li><li>Create &amp; Execute SQL Statement</li><li>Describe Result Set Object</li><li>Define and Create Stored Procedure</li></ul></li></ul></div><div class="col-md-3"><ul type="square"><li><b class="text-danger">Input/output Stream  </b><ul><li>Java I/O Classes and Interfaces</li><li>File I/O</li><li>Byte I/O</li><li>Piped I/O</li><li>Filter I/O</li><li>Object I/O</li><li>Serialization</li></ul></li> </ul><ul type="square"><li><b class="text-danger">Network Programming</b><ul><li>Understanding how the internet works</li><li>Explain client-server computing</li><li>Describe the classes of the java.net package</li><li>Describe java’s web related classes</li></ul></li></ul></div> <div class="col-md-3"><ul type="square"><li><b class="text-danger">Remote Method Invocation (RMI)</b><ul><li>Describe distributed applications</li><li>Build distributed applications</li><li>Define RMI</li><li>Outline the Java distributed model</li><li>List the java.rmi package</li><li>Explain the 3-tier layering of Java RMI</li><li>Implement RMI on a remote &amp; local host</li><li>Describe remote objects</li><li>Inner classes, Sockets, Web connectivity, Security</li></ul></li></ul></div>   </div>                 <div class="row"><div class="col-md-3"><ul type="square"><li><b class="text-danger">Java Beans</b><ul><li>Define Java Beans</li><li>Describe Software Component Model</li><li>Understand BDK</li><li>List the tools for bean development</li><li>Create your own bean</li><li>Describe the custom bean properties and events</li><li>Understand introspection Report</li><li>Implement various types of properties</li><li>Describe event listener</li><li>List the benefits of using Java Beans</li></ul></li></ul><ul type="square"><li><b class="text-danger">JSF</b><ul><li>Introduction to JSF</li><li>JSF Features and Life Cycle</li><li>JSF Managed Beans</li><li>JSF UI Components</li><li>JSF Validations</li></ul></li></ul></div><div class="col-md-3"><ul type="square"><li><b class="text-danger">Enterprise java Beans (EJB)</b><ul><li>Introduction to EJB</li><li>Types of EJB</li><li>Session Beans-Stateless Session Beans, Stateful Session Beans</li><li>Entity Beans</li><li>Message Driven Beans</li></ul></li></ul><ul type="square"><li><b class="text-danger">Servlets</b><ul><li>Define &amp; Compile a Servlet</li><li>Life Cycle of a Servlet</li><li>Describe an HTTP Servlet</li><li>Use a Servlet to retrieve information</li><li>Define Session Tracking</li><li>Describe InterServlet Communication</li><li>Use a Servlet to access a database</li></ul></li></ul></div><div class="col-md-3"><ul type="square"><li><b class="text-danger">Java Server Page (JSP)</b><ul><li>Basic of JSP</li><li>Directives (page, include, taglib, etc.)</li><li>Scripting Elements</li><li>Life Cycle of JSP</li><li>JSP &amp; Java Beans</li><li>Error Handling in JSP</li><li>JSP with JDBC</li></ul></li></ul><ul type="square"><li><b class="text-danger">Hibernate</b><ul><li>What is the problem with JDBC-paradigm mismatch</li><li>What is ORM?</li><li>Understanding different components of Hibernate</li><li>How to persist objects using Hibernate</li><li>How to use mapping files, configuration files and Session Object</li><li>Instance States</li><li>How to generate ID</li><li>How to implement Inheritance in Hibernate</li><li>Working with relationship between entities</li><li>Transactions in Hibernate</li><li>Querying with HQL (Hibernate Query Language)</li></ul></li></ul></div><div class="col-md-3"><ul type="square"><li><b class="text-danger">Struts</b><ul><li>Introduction to Struts</li><li>Overview of Model, view, Controller(MVC) design pattern</li><li>How the Struts Framework applies MVC</li><li>How request are handled in Struts</li><li>Struts main components</li><li>The Controller components</li><li>Struts configuration files</li><li>The View Components</li><li>Internationalization and multiple languages views</li><li>The Model Components</li><li>Struts validator framework.</li></ul></li></ul></div></div><div class="row"><div class="col-md-12"><ul><li><b class="text-success">Career Prospectus</b>-Advance Java professionals can find the opportunities in any of the following category<ul><li>Java Programmer</li><li>Web Developer</li></ul></li></ul></div></div>         
              <div class="clearfix mt-1"></div>
                         
                               <div>
                                <h4>Share It</h4>
                                 <a href='https://twitter.com/intent/tweet?url=http://dicsindia.in/course/advance-java/15&text=Advance Java&via=dicsindia'><i class="fa fa-twitter-square fa-2x"></i></a>
                                 <a href='https://facebook.com/sharer.php?u=http://dicsindia.in/course/advance-java/15'><i class="fa fa-facebook-official fa-2x"></i></a>
                                 <a href='https://web.whatsapp.com//send?text=Advance Java http://dicsindia.in/course/advance-java/15'><i class="fa fa-whatsapp fa-2x"></i></a>
                              </div>
        
         
        
   </div>
 </div>
</div>

        </div>
      </div>
    </section>
    <!--//Team-->
     <!--footer-->
     <?php include 'footer.php'  ?>