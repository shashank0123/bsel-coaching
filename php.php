<!--A Design by Techlabz Infosoft Pvt Ltd
  Author: Techlabz Infosoft Pvt.Ltd
  Author URL: http://www.techlabzinfosoft.com
 
  -->
<!DOCTYPE html>
<html lang="zxx">
  <head>
    <title>The Bsel| Home </title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <script>
      addEventListener("load", function () {
      	setTimeout(hideURLbar, 0);
      }, false);
      
      function hideURLbar() {
      	window.scrollTo(0, 1);
      }
    </script>
    <!--//meta tags ends here-->
    <!--booststrap-->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- //font-awesome icons -->
    <link href="css/blast.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style10.css" />
    <!--stylesheets-->
     <link href="css/main.css" rel='stylesheet' type='text/css' media="all">
    <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Merriweather:300,400,700,900" rel="stylesheet">
  </head>
  <body>
    <div class="blast-box">
      <div class="blast-icon"><span class="fas fa-tint"></span></div>
      <div class="blast-frame">
        <p>change colors</p>
        <div class="blast-colors">
          <div class="blast-color">#86bc42</div>
          <div class="blast-color">#8373ce</div>
          <div class="blast-color">#14d4f4</div>
          <div class="blast-color">#72284b</div>
        </div>
        <p class="blast-custom-colors">Custom colors</p>
        <input type="color" name="blastCustomColor" value="#cf2626">
      </div>
    </div>
    
    <div class="header-outs" id="header">
      <div class="header-w3layouts">
        <div class="container">
          <div class="row headder-contact">
            <div class="col-lg-6 col-md-7 col-sm-9 info-contact-agile">
              <ul>
                <li>
                  <span class="fas fa-phone-volume" ></span>
                  <p>+91 98354 29138</p>
                </li>
                <li>
                  <span class="fas fa-envelope"></span>
                  <p><a href="mailto:info@example.com">info@thebsel.in</a></p>
                </li>
                <li>
                <span class="fas fa-user"></span>
                    <p>  <a href="login.php"  >Student login</a></p>
                   
                </li>
              </ul>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-3 icons px-0">
              <ul>
                <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                
                <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                
              </ul>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="hedder-up">
            <h1 ><a href="index.php" class="navbar-brand" data-blast="color">Thebsel</a></h1>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="headdser-nav-color" data-blast="bgColor">
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
              <ul class="navbar-nav ">
                <li class="nav-item active">
                  <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                
                <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Why Us
                                
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="mission-vission.php">Mission/Vission</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="massage-director.php">Director Massege</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="about.php">About</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="Franchies.php">Franchisee</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="branch.php">Branches</a>


                            </div>
                        </li>

                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Our Cources
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="dca.php">DCA</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="dtp.php">DTP</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="adca.php">ADCA</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="tally.php">TALLY</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="c.php">C Language</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="php.php">PHP Language</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="java.php">JAVA</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="oracle.php">ORACLE</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="Webdesign.php">Webdesign</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="english.php">English</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="computer-teacher-training-course.php">Computer Training</a>

                            </div>
                        </li>
                 <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Academics
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="top-faculties.php">Top Faculties</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="batch.php">Batch Schedules</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="result.php">Result</a>

                            </div>
                        </li>
                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Gallery
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="gallery-photo.php">Photo Gallery</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="gallery-video.php">Video Gallery</a>

                            </div>
                        </li>
                        
                
                <li class="nav-item">
                  <a href="contact.php" >Contact</a>
                </li>
                 
              </ul>
            </div>
          </div>
        </nav>
        <!--//navigation section -->
        <div class="clearfix"> </div>
      </div><!-- banner -->
      
  <img src="images/cc6.jpg" class="img-responsive">

<!-- //banner -->
<!-- banner1 -->
	

	<div class="services-breadcrumb">
		<div class="container">
			<p><a href="index.php"  data-toggle="modal" class="info" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Home</a>|About</p>
		</div>
	</div>
	
<!-- //banner1 -->

<!--Team-->
    <section class="team py-lg-4 py-md-3 py-sm-3 py-3" id="team">
      <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
        <h3 class="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">PHP Web Development </h3>
        <div class="row ">
          <!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header header-bg  text-center font-weight-bold">
        
        <h2 class="modal-title"><i class="fa fa-headphones"></i> Course Enquiry Form</h2>
                         
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
          <div class="card">
                     
                         <div class="card-body footer-bg">
                             <div class="form-group">
                                
                                 <input name="ctl00$ContentPlaceHolder1$txtenquiryname" type="text" id="ContentPlaceHolder1_txtenquiryname" class="form-control" placeholder="Enter Your Name" />
                             </div>
                             <div class="form-group">
                               
                                 <input name="ctl00$ContentPlaceHolder1$txtenquiryemail" type="email" id="ContentPlaceHolder1_txtenquiryemail" class="form-control" placeholder="Enter Your Email" />
                             </div>
                             <div class="form-group">
                               
                                 <input name="ctl00$ContentPlaceHolder1$txtenquiryphone" type="text" id="ContentPlaceHolder1_txtenquiryphone" class="form-control" placeholder="Enter Your Mobile No" />
                             </div>
                             <div class="form-group">
                               
                                 <select name="ctl00$ContentPlaceHolder1$ddlbranches" id="ContentPlaceHolder1_ddlbranches" class="custom-select">
  <option value="Any">Select Your Preferred DICS branch</option>
  <option value="1">DICS GTB Nagar,Kingsway Camp</option>
  <option value="2">DICS Pitampura</option>
  <option value="3">DICS Punjabi Bagh</option>
  <option value="4">DICS Raja Garden</option>
  <option value="5">DICS Janakpuri</option>
  <option value="6">DICS Karol Bagh</option>
  <option value="7">DICS Faridabad</option>
  <option value="8">DICS Ghaziabad</option>
  <option value="9">DICS Panipat</option>
  <option value="10">DICS Preet Vihar</option>

</select>
                             </div>
                             <div class="form-group">
                               
                                 <select name="ctl00$ContentPlaceHolder1$ddlcourses" id="ContentPlaceHolder1_ddlcourses" class="custom-select">
  <option value="Any">Select Your Preferred Course</option>
  <option value="30">Certificate in Computer Administration and Management (CCAM)</option>
  <option value="29">Management Information System (MIS)</option>
  <option value="28">Microsoft Certified System Engineer (MCSE)</option>
  <option value="27">Cisco Certified Network Associate (CCNA)</option>
  <option value="26">Master in Animation</option>
  <option value="25">Multimedia Studio</option>
  <option value="24">Advance Diploma in Financial Accounting</option>
  <option value="23">Advance Diploma in Computer Hardware and Networking (ADCHN)</option>
  <option value="22">Diploma in Computer Hardware and Networking (DCHN)</option>
  <option value="21">C Language</option>
  <option value="20">Digital Marketing</option>
  <option selected="selected" value="19">PHP Web Development</option>
  <option value="18">Diploma in Web Designing and Promotion</option>
  <option value="17">Programming in Python</option>
  <option value="16">Microsoft .NET</option>
  <option value="15">Advance Java</option>
  <option value="14">Web Studio</option>
  <option value="13">OOPS through Java (Core)</option>
  <option value="12">Android Mobile App Development</option>
  <option value="11">Master Diploma in Software Engineering (MDSE)</option>
  <option value="10">Master Diploma in Application Development (MDAD)</option>
  <option value="9">Professional Diploma in Application Development (PDAD)</option>
  <option value="8">Graphic Studio</option>
  <option value="7">Professional Diploma in Software Engineering (PDSE)</option>
  <option value="6">DOEACC A Level / Advance Diploma in Software Engineering (ADSE)</option>
  <option value="5">Certificate in Financial Accounting (CIFA)</option>
  <option value="4">Diploma in Financial Accounting (DIFA)</option>
  <option value="3">DOEACC O Level / Diploma in Computer Business Application (DCBA)</option>
  <option value="1">DOEACC CCC / Basic Computer Concept (BCC)</option>

</select>
                             </div>
                             <div class="form-group">
                               
                                 <textarea name="ctl00$ContentPlaceHolder1$txtmessage" rows="2" cols="20" id="ContentPlaceHolder1_txtmessage" class="form-control" placeholder="Write your query">
</textarea>
                             </div>
                             <small class="clearfix">By clicking on Request Callback, I allow DICS to contact , use & share my personal data as per the <a href="#">Privacy Policy.</a></small>
                            
                             <input type="submit" name="ctl00$ContentPlaceHolder1$btnsend" value="Request Callback" id="ContentPlaceHolder1_btnsend" class="btn btn-warning" />
                             
                         </div>

                        
                     
                 </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>

</div>

 <div class="container-fluid mt-1 mb-1">
          <div class="row">
              <div class="col-md-3">
                 
                  
        <ul class="list-group">
            <li class="list-group-item bg-warning text-dark font-weight-bold"> <i class="fa fa-book"></i> Our Courses</li>
    
        
                  <li class="list-group-item bold-text bg-dark"><a href='dca.php' class="text-white"><i class="fa fa-cog"></i> DCA</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='dtp.php' class="text-white"><i class="fa fa-cog"></i>DTP</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='adca.php' class="text-white"><i class="fa fa-cog"></i> ADCA</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='tally.php' class="text-white"><i class="fa fa-cog"></i>TALLY</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='c.php' class="text-white"><i class="fa fa-cog"></i> C, C++</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='java.php' class="text-white"><i class="fa fa-cog"></i> JAVA,</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='oracle.php' class="text-white"><i class="fa fa-cog"></i>, ORAcle</a></li>
                   <li class="list-group-item bold-text bg-dark"><a href='php.php' class="text-white"><i class="fa fa-cog"></i>, PHP</a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='computer-teacher-training-course.php' class="text-white"><i class="fa fa-cog"></i> computer training </a></li>
                     
                     
                 
    
        
                   <li class="list-group-item bold-text bg-dark"><a href='english.php' class="text-white"><i class="fa fa-cog"></i> English</a></li>
                     
                     
                 
    
         </ul>
    

                 
              </div>
               <div class="col-md-9">
                  
    

        
              <h2 class="text-danger font-weight-bold"> PHP Web Development</h2>
               <hr class="hr-yellow"/>
               <p class="text-justify">
                 <a href='/docs/courses/' class="btn btn-success">Download Syllabus <i class="fa fa-download"></i></a>  <a href="#" class="btn btn-warning"  data-toggle="modal" data-target="#myModal">Callback Request <i class="fa fa-envelope-open" aria-hidden="true"></i></a> 
                  <div class="clearfix"></div>
                 <strong>Duration : </strong> 6 Months  /  <strong>Type :</strong> Certificate / <strong>Mode : </strong> Part Time  
                  
                 
               </p>
              
                <p class="text-justify"><b>Objective :</b>The objective of the course is to enable a student to acquire the advance knowledge pertaining to PHP Web Development.</p><p class="text-justify"><b>Eligibility/Prerequisite : </b>10+2 / ITI (1 Year) </p>               <h2 class="text-primary"> <i class="fa fa-cog"></i> Course Structure  </h2> <div class="row"><div class="col-md-6"><ul type="square"><li><b class="text-danger">Web Page Layout Using HTML &amp; CSS</b><ul><li>Introduction of HTML</li><li>HTML Tags and their properties</li><li>Introduction of CSS &amp; their types</li><li>CSS properties and values</li><li>Web page layout using HTML &amp; CSS</li><li>Responsive Web Layout Using Bootstrap</li></ul></li></ul></div><div class="col-md-6"><ul type="square"><li><b class="text-danger">Client Side Programming Using JavaScript</b><ul><li>Introduction to Java Script</li><li>JavaScript Tokens-Identifiers</li><li>Keywords, Literals, Operators, Symbols</li><li>Java Script Functions- Built-in-functions&amp; User-defined functions</li><li>Using Objects and Events</li><li>Using Java Script in Website</li><li>Asynchronous Web Application using AJAX</li><li>JSON format  for storing and transporting data</li></ul></li> </ul></div><div class="row"><div class="col-md-6"><ul type="square"><li><b class="text-danger">Custom Animated Design using jQuery</b><ul><li>JQuery library</li><li>Scrolling effects</li><li>Images and Forms Ajax Integration</li><li>Fading practical</li><li>Animation effect practical</li></ul></li></ul></div><div class="col-md-6"><ul type="square"><li><b class="text-danger">Database Management System using MySQL</b><ul><li>An overview of Database Management System</li><li>An Architecture of the Database Management System</li><li>Relational Database Management System and CODD's Rule</li><li>E-R Modeling and Normalization</li><li>MySQL Overview<br></li><li>MySQL Databases<br></li><li>Tables and Views<br></li><li>MySQL Queries<br></li><li>MySQL Clauses<br></li><li>MySQL Conditions</li><li>MySQL Joins</li><li>Aggregate Functions<br></li></ul></li></ul></div></div><div class="row"><div class="col-md-6"><ul type="square"><li><b class="text-danger">Introduction to PHP</b><ul><li>Basic Knowledge of websites</li><li>Introduction of Dynamic website</li><li>Introduction to PHP</li><li>Why and Scope of PHP</li><li>XAMPP and WAMP Installation</li></ul></li></ul><ul type="square"><li><b class="text-danger">PHP Functions</b><ul><li>PHP Functions</li><li>Creating an Array</li><li>Modifying Array elements</li><li>Processing Array with Loops</li><li>Grouping Form Selections with Arrays</li><li>Using Array Functions</li><li>User predefined PHP Functions</li><li>Crating User-Defined Functions</li></ul></li></ul><ul type="square"><li><b class="text-danger">CMS Using Wordpress</b><ul><li>Installation &amp; Configuration of Wordpress</li><li>Theme Integration</li><li>Manage Widgets, Plug-Ins</li><li>Adding Pages and Posts</li><li>Project in Word press</li></ul></li></ul></div><div class="col-md-6"><ul type="square"><li><b class="text-danger">PHP Programming Basics</b><ul><li>Syntax of PHP</li><li>Embedding PHP in HTML</li><li>Embedding HTML in PHP</li><li>Introduction to PHP variable</li><li>Understanding Data Types</li><li>Using Operators</li><li>Using Conditional Statements</li><li>If(), else if(), and else if condition statement</li><li>Switch() Statements</li><li>Using the While() Loop</li><li>Using the for() Loop</li></ul></li> </ul><ul type="square"><li><b class="text-danger">Advance PHP (PHP++)</b><ul><li>File Handling using PHP</li><li>Session</li><li>Cookies</li><li>Dealing with Dates and Times</li><li>Executing External Programs</li><li>Object Oriented Programming Concept with PHP</li><li>Exceptional Handling</li><li>PHP-AJAX</li><li>PHP-XML</li></ul></li> </ul><ul type="square"><li><b class="text-danger">Web Deployment</b><ul><li>Managing Domains &amp; Hosting</li><li>Working with CPanel</li><li>Using FTP for File Transfer</li></ul></li></ul></div></div><div class="row"><div class="col-md-12"><ul type="square"><li><b class="text-success">Career Prospects :-PHP Web Deveopment professionals can find the opportunities in any of the following category- </b><ul><li>Web/UI Designer</li><li>Web/UI Developer</li></ul></li></ul></div></div></div>
              <div class="clearfix mt-1"></div>
                         
                               <div>
                                <h4>Share It</h4>
                                 <a href='https://twitter.com/intent/tweet?url=http://dicsindia.in/course/php-web-development/19&text=PHP Web Development&via=dicsindia'><i class="fa fa-twitter-square fa-2x"></i></a>
                                 <a href='https://facebook.com/sharer.php?u=http://dicsindia.in/course/php-web-development/19'><i class="fa fa-facebook-official fa-2x"></i></a>
                                 <a href='https://web.whatsapp.com//send?text=PHP Web Development http://dicsindia.in/course/php-web-development/19'><i class="fa fa-whatsapp fa-2x"></i></a>
                              </div>
        
         
        
   </div>
 </div>
</div>

        </div>
      </div>
    </section>
    <!--//Team-->
     <!--footer-->
     <?php include 'footer.php'  ?>