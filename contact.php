<!--A Design by Techlabz Infosoft Pvt Ltd
  Author: Techlabz Infosoft Pvt.Ltd
  Author URL: http://www.techlabzinfosoft.com
 
  -->
<!DOCTYPE html>
<html lang="zxx">
  <head>
    <title>The Bsel || Contact </title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <script>
      addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
      }, false);
      
      function hideURLbar() {
        window.scrollTo(0, 1);
      }
    </script>
    <!--//meta tags ends here-->
    <!--booststrap-->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet" type="text/css" media="all">
    <!-- //font-awesome icons -->
    <link href="css/blast.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style10.css" />
    <!--stylesheets-->
    <link href="css/style.css" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Merriweather:300,400,700,900" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script>
function sendContact() {
  var valid;  
  valid = validateContact();
  if(valid) {
    jQuery.ajax({
    url: "contact_mail.php",
    data:'userName='+$("#userName").val()+'&userEmail='+$("#userEmail").val()+'&subject='+$("#subject").val()+'&content='+$(content).val(),
    type: "POST",
    success:function(data){
    $("#mail-status").html(data);
    },
    error:function (){}
    });
  }
}

function validateContact() {
  var valid = true; 
  $(".demoInputBox").css('background-color','');
  $(".info").html('');
  
  if(!$("#userName").val()) {
    $("#userName-info").html("(required)");
    $("#userName").css('background-color','#FFFFDF');
    valid = false;
  }
  if(!$("#userEmail").val()) {
    $("#userEmail-info").html("(required)");
    $("#userEmail").css('background-color','#FFFFDF');
    valid = false;
  }
  if(!$("#userEmail").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) {
    $("#userEmail-info").html("(invalid)");
    $("#userEmail").css('background-color','#FFFFDF');
    valid = false;
  }
  if(!$("#subject").val()) {
    $("#subject-info").html("(required)");
    $("#subject").css('background-color','#FFFFDF');
    valid = false;
  }
  if(!$("#content").val()) {
    $("#content-info").html("(required)");
    $("#content").css('background-color','#FFFFDF');
    valid = false;
  }
  
  return valid;
}
</script>
  </head>
  <body>
    <div class="blast-box">
      <div class="blast-icon"><span class="fas fa-tint"></span></div>
      <div class="blast-frame">
        <p>change colors</p>
        <div class="blast-colors">
          <div class="blast-color">#86bc42</div>
          <div class="blast-color">#8373ce</div>
          <div class="blast-color">#14d4f4</div>
          <div class="blast-color">#72284b</div>
        </div>
        <p class="blast-custom-colors">Custom colors</p>
        <input type="color" name="blastCustomColor" value="#cf2626">
      </div>
    </div>
    
    <div class="header-outs" id="header">
      <div class="header-w3layouts">
        <div class="container">
          <div class="row headder-contact">
            <div class="col-lg-6 col-md-7 col-sm-9 info-contact-agile">
              <ul>
                <li>
                  <span class="fas fa-phone-volume" ></span>
                  <p>+91 98354 29138</p>
                </li>
                <li>
                  <span class="fas fa-envelope"></span>
                  <p><a href="mailto:info@example.com">info@thebsel.in</a></p>
                </li>
                <li>
                
                      <a href="login.php" >Student login</a>
                   
                </li>
              </ul>
            </div>
            <div class="col-lg-6 col-md-5 col-sm-3 icons px-0">
              <ul>
                <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
                
                <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                
              </ul>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="hedder-up">
            <h1 ><a href="index.php" class="navbar-brand" data-blast="color"><img src="images/logobsel.jpeg" class="img-responsive" width="200px;"></a></h1>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
          </button>
          <div class="headdser-nav-color" data-blast="bgColor">
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
              <ul class="navbar-nav ">
                <li class="nav-item active">
                  <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                
                <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Why Us
                                
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="mission-vission.php">Mission/Vission</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="massage-director.php">Director Massege</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="about.php">About</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="Franchies.php">Franchisee</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="branch.php">Branches</a>


                            </div>
                        </li>
                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Our Cources
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="dca.php">DCA</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="dtp.php">DTP</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="adca.php">ADCA</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="tally.php">TALLY</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="c.php">C Language</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="php.php">PHP Language</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="java.php">JAVA</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="oracle.php">ORACLE</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="Webdesign.php">Webdesign</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="english.php">English</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="computer-teacher-training-course.php">Computer Training</a>

                            </div>
                        </li>
                 <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               Academics
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="top-faculties.php">Top Faculties</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="batch.php">Batch Schedules</a>
                                 <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="result.php">Result</a>

                            </div>
                        </li>
                         <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Gallery
                               
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="gallery-photo.php">Photo Gallery</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="gallery-video.php">Video Gallery</a>

                            </div>
                        </li>
                        
                
                <li class="nav-item">
                  <a href="contact.php" >Contact</a>
                </li>
                 
              </ul>
            </div>
          </div>
        </nav>
        <!--//navigation section -->
        <div class="clearfix"> </div>
      </div><!--contact -->
       <img src="images/cc6.jpg" class="img-responsive">

<!-- //banner -->
<!-- banner1 -->
  

  <div class="services-breadcrumb">
    <div class="container">
      <p><a href="index.php"  data-toggle="modal" class="info" data-toggle="modal" data-target="#exampleModalLive" data-blast="bgColor">Home</a>|Contact</p>
    </div>
  </div>
    <section class="contact py-lg-4 py-md-3 py-sm-3 py-3" id="contact">
      <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
        <h3 class="title clr text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">Contact Us</h3>
        <div class="row">
          <div class="col-md-5 address-grid">
            <div class="addres-office-hour text-center" >
              <ul>
                <li class="mb-2">
                  <h6 data-blast="color">Address</h6>
                </li>
                <li>
                  <p>Savitri complex bhagwanpur chatti,<br> near yadav nagar gate,<br> rewa road, muzaffarpur(bihar)</p>
                </li>
              </ul>
              <ul>
                <li class="mt-lg-4 mt-3">
                  <h6 data-blast="color">Phone</h6>
                </li>
                <li class="mt-2">
                  <p> 8294337201, 09835429138</p>
                </li>
                <li class="mt-lg-4 mt-3">
                  <h6 data-blast="color">Email</h6>
                </li>
                <li class="mt-2">
                  <p><a href="mailto:info@example.com">thebsel@gmail.com </a></p>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-7 contact-form">
            <form >
              <div class="row text-center contact-wls-detail">
                <div class="col-md-6 form-group contact-forms">
                  <input type="text" name="userName" class="form-control" placeholder="Your Name" required="">
                </div>
                <div class="col-md-6 form-group contact-forms">
                  <input type="email" name="userEmail" id="userEmail" class="form-control" placeholder="Your Email" required="">
                </div>
              </div>
              <div class="form-group contact-forms">
                <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject" required="">
              </div>
              <div class="form-group contact-forms">
                <textarea class="form-control" rows="3" name="content" id="content" placeholder="Your Message" required=""></textarea>
              </div>
              <div class="sent-butnn text-center">
                <button type="submit" name="submit" class="btn btn-block" data-blast="bgColor" onClick="sendContact();">Send</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!--//contact -->
    <!--footer-->
     <?php include 'footer.php'  ?>