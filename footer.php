  <!--footer-->
    <section class="footer py-lg-4 py-md-3 py-sm-3 py-3">
      <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
        <div class="row ">
          <div class="dance-agile-info col-lg-3 col-md-6 col-sm-6">
            <h4 class="pb-lg-3 pb-3" data-blast="color">Our Cources</h4>
            <div class="footer-grid row mb-3">
             
              <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="dca.php" data-toggle="modal" data-target="#exampleModalLive">DCA</a></h6>
              </div>
            </div>
            <div class="footer-grid row mb-3">
              <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="dtp.php" data-toggle="modal" data-target="#exampleModalLive">ADIT</a></h6>
              </div>
            </div>
            <div class="footer-grid row mb-3">
              <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="adca.php" data-toggle="modal" data-target="#exampleModalLive">ADCA</a></h6>
              </div>
              </div>
              <div class="footer-grid row mb-3">
               <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="tally.php" data-toggle="modal" data-target="#exampleModalLive">TALLY</a></h6>
              </div>
              </div>
              <div class="footer-grid row mb-3">
               <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="c.php" data-toggle="modal" data-target="#exampleModalLive">C/C++</a></h6>
              </div>
              </div>
             
              <div class="footer-grid row mb-3">
               <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="java.php" data-toggle="modal" data-target="#exampleModalLive">JAVA</a></h6>
                
              </div>
              </div>
              <div class="footer-grid row mb-3">
              <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="computer-teacher-training-cources.php" data-toggle="modal" data-target="#exampleModalLive">Computer training </a></h6>
                
              </div>
              </div>
              <div class="footer-grid row mb-3">
              <div class="col-lg-7 col-md-6 col-sm-6 col-7 bottom-para px-0">
                <h6><a href="english.php" data-toggle="modal" data-target="#exampleModalLive">English </a></h6>
                
              </div>
            </div>
          </div>
          <div class="dance-agile-info col-lg-3 col-md-6 col-sm-6">
            <h4 class="pb-lg-4 pb-md-3 pb-3 text-uppercase" data-blast="color">Contact Us</h4>
            <div class="address_mail_footer_grids">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3582.3020931426504!2d85.34931951450359!3d26.121691399982584!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ed1732ad8bbaef%3A0x1d05285e49210a0b!2sTHE+BSEL!5e0!3m2!1sen!2sin!4v1563300084022!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
          <div class="dance-agile-info col-lg-3 col-md-6 col-sm-6">
            <h4 class="pb-lg-4 pb-md-3 pb-3 text-uppercase" data-blast="color">Address</h4>
            <div class="footer-office-hour">
              <ul>
                <li>
                  <p>Savitri complex bhagwanpur chatti, near yadav nagar gate, rewa road, muzaffarpur(bihar)

</p>
                </li>
                <li>
                  <p>Help Line </p>
                </li>
                <li class="my-1">
                  <p><a href="mailto:info@example.com" data-blast="color">8294337201, 09835429138</a></p>
                </li>
                
                <li>
                  <p>Email: </p>
                </li>
                <li class="my-1">
                  <p><a href="mailto:info@example.com" data-blast="color">thebsel@gmail.com</a></p>
                </li>
               
              </ul>
            </div>
          </div>
          <div class="dance-agile-info col-lg-3 col-md-6 col-sm-6">
            <h4 class="pb-lg-3 pb-3" data-blast="color">Facebook</h4>
            <div class="footer-post d-flex mb-2">
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class=" scroll">
                <img src="images/f1.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class="scroll">
                <img src="images/f2.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1">
                <a href="#blog" class="scroll">
                <img src="images/f3.jpg" alt=" " class="img-fluid">
                </a>
              </div>
            </div>
            <div class="footer-post d-flex mb-2">
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class="scroll">
                <img src="images/f3.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class=" scroll">
                <img src="images/f2.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1">
                <a href="#blog" class=" scroll">
                <img src="images/f1.jpg" alt=" " class="img-fluid">
                </a>
              </div>
            </div>
            <div class="footer-post d-flex">
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class="scroll">
                <img src="images/f2.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1 mr-2">
                <a href="#blog" class=" scroll">
                <img src="images/f3.jpg" alt=" " class="img-fluid">
                </a>
              </div>
              <div class="agileinfo_footer_grid1">
                <a href="#blog" class=" scroll">
                <img src="images/f1.jpg" alt=" " class="img-fluid">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="nav-footer py-sm-4 py-3">
      <div class="container-fluid">
        <div class="buttom-nav ">
          <nav class="border-line py-2">
            <ul class="nav justify-content-center">
              <li class="nav-item active">
                <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a href="#about" class="nav-link scroll">About</a>
              </li>
              
              
              <li class="nav-item">
                <a href="#contact" class="nav-link scroll">Contact</a>
              </li>
              <li>
                <div class="outs_more-buttn" >
                      <a href="admin/index.php"  >Admin</a>
                    </div>
                </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
    <footer class="py-3">
      <div class="container">
        <div class="copy-agile-right text-center">
          <div class="list-social-icons text-center py-lg-4 py-md-3 py-3">
            <ul>
              <li><a href="#"><span class="fab fa-facebook-f"></span></a></li>
              
              <li><a href="#"><span class="fab fa-youtube"></span></a></li>
            </ul>
          </div>
          <p> 
            © 2018 ClassWork. All Rights Reserved | Design by <a href="http://www.techlabzinfosoft.com" target="_blank">Techlabz Infosoft Pvt Ltd</a>
          </p>
        </div>
      </div>
    </footer>
    <!--//footer-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script> 
    <script>$(document).ready(function() {  

    var id = '#dialog';
  
    //Get the screen height and width
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();
  
    //Set heigth and width to mask to fill up the whole screen
    $('#mask').css({'width':maskWidth,'height':maskHeight});
    
    //transition effect   
    $('#mask').fadeIn(500); 
    $('#mask').fadeTo("slow",0.9);  
  
    //Get the window height and width
    var winH = $(window).height();
    var winW = $(window).width();
              
    //Set the popup window to center
    $(id).css('top',  winH/2-$(id).height()/2);
    $(id).css('left', winW/2-$(id).width()/2);
  
    //transition effect
    $(id).fadeIn(2000);   
  
  //if close button is clicked
  $('.window .close').click(function (e) {
    //Cancel the link behavior
    e.preventDefault();
    
    $('#mask').hide();
    $('.window').hide();
  });   
  
  //if mask is clicked
  $('#mask').click(function () {
    $(this).hide();
    $('.window').hide();
  });   
  
});</script>
    <!--js working-->
    <script src='js/jquery-2.2.3.min.js'></script>
    <!--//js working--> 
    <!--blast colors change-->
    <script src="js/blast.min.js"></script>
    <!--//blast colors change-->
    <!--responsiveslides banner-->
    <script src="js/responsiveslides.min.js"></script>
    <script>
      // You can also use "$(window).load(function() {"
      $(function () {
        // Slideshow 4
        $("#slider4").responsiveSlides({
          auto: true,
          pager:false,
          nav:true ,
          speed: 900,
          namespace: "callbacks",
          before: function () {
            $('.events').append("<li>before event fired.</li>");
          },
          after: function () {
            $('.events').append("<li>after event fired.</li>");
          }
        });
      
      });
    </script>
    <!--// responsiveslides banner-->     
    <!--responsive tabs-->   
    <script src="js/easy-responsive-tabs.js"></script>
    <script>
      $(document).ready(function () {
      $('#horizontalTab').easyResponsiveTabs({
      type: 'default', //Types: default, vertical, accordion           
      width: 'auto', //auto or any width like 600px
      fit: true,   // 100% fit in a container
      closed: 'accordion', // Start closed if in accordion view
      activate: function(event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#tabInfo');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
      }
      });
      });
       
    </script>
    <!--// responsive tabs--> 
    <!--About OnScroll-Number-Increase-JavaScript -->
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.countup.js"></script>
    <script>
      $('.counter').countUp();
    </script>
    <!-- //OnScroll-Number-Increase-JavaScript -->    
    <!-- start-smoth-scrolling -->
    <script src="js/move-top.js"></script>
    <script src="js/easing.js"></script>
    <script>
      jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
          event.preventDefault();
          $('html,body').animate({
            scrollTop: $(this.hash).offset().top
          }, 900);
        });
      });
    </script>
    <!-- start-smoth-scrolling -->
    <!-- here stars scrolling icon -->
    <script>
      $(document).ready(function () {
      
        var defaults = {
          containerID: 'toTop', // fading element id
          containerHoverID: 'toTopHover', // fading element hover id
          scrollSpeed: 1200,
          easingType: 'linear'
        };
      
      
        $().UItoTop({
          easingType: 'easeOutQuart'
        });
      
      });
    </script>
     

    <!-- //here ends scrolling icon -->
    <!--bootstrap working-->
    <script src="js/bootstrap.min.js"></script>
    <!-- //bootstrap working-->
  </body>
</html>