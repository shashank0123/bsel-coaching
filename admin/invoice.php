<?php
include("../library/raso_function.php");
check_admin();
$errormsg = '';
$action = "add";

$id="";
$emailid='';
$rollno='';
$regno='';
$sname='';
$fname='';
$joindate = '';
$courses = '';
$dob = '';
$remark='';
$contact='';
$password='';
$balance = 0;
$fees='';

$branch='';

$sel_user=exeQuery("select * from admin where id='".$_SESSION['id']."' ");
$res_user=fetchAssoc($sel_user);

$sel_prefix=exeQuery("select * from branch where id='".$res_user['branch']."' ");
$res_prefix=fetchAssoc($sel_prefix);

$prefix=$res_prefix['branch_name'];
$prefix=substr($prefix,0,2);

$sel_reno=exeQuery("select * from student where branch='".$res_user['branch']."' order by reno desc ");
$res_reno=fetchAssoc($sel_reno);


if(isset($_POST['save']))
{
	$regno = addslashes($_POST['regno']);
	$joindate = addslashes($_POST['joindate']);
	$courses = addslashes($_POST['course_id']);
	$remark = addslashes($_POST['remark']);
	$fees = addslashes($_POST['fees']);
	$advancefees = addslashes($_POST['advancefees']);
	$balance = $fees-$advancefees;

	$regno=end(explode("-",$regno));

	
			$q1=exeQuery("INSERT INTO fees_transaction set stdid='".$_REQUEST['id']."',paid='".$advancefees."',fees='".$fees."',course_id='".$courses."',balance='".$balance."',submitdate='".date('Y-m-d')."',transcation_remark='".$remark."'  " );
			

			$q2=exeQuery("select * from ".TABLE_STUDENT." where id='".$_REQUEST['id']."' ");
			$res_q2=fetchAssoc($q2);
			$balance=$res_q2['balance']-$advancefees;

			$update_bal=exeQuery("update  ".TABLE_STUDENT." set balance='".$balance."' where id='".$_REQUEST['id']."' ");
		
		echo '<script type="text/javascript">window.location="invoice.php?act=1";</script>';

	}



if(isset($_GET['action']) && $_GET['action']=="delete"){

	$delete=exeQuery("delete from  student WHERE Id='".$_GET['id']."'");	
	header("location: invoice.php?act=3");

}


$action = "add";
if(isset($_GET['action']) && $_GET['action']=="edit" ){
	$id = isset($_GET['id'])?addslashes($_GET['id']):'';

	$sqlEdit = exeQuery("SELECT * FROM student WHERE Id='".$id."'");
	$res_edit=fetchAssoc($sqlEdit);
// if($sqlEdit->num_rows)
// {
// $rowsEdit = $sqlEdit->fetch_assoc();
// extract($rowsEdit);
// $action = "update";
// }else
// {
// $_GET['action']="";
// }

}


if(isset($_REQUEST['act']) && @$_REQUEST['act']=="1")
{
	$errormsg = "<div class='alert alert-success'> <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong> Student Add successfully</div>";
}else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="2")
{
	$errormsg = "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> <strong>Success!</strong> Student Edit successfully</div>";
}
else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="3")
{
	$errormsg = "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong> Student Delete successfully</div>";
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo SITENAME;?></title>

	<!-- BOOTSTRAP STYLES-->
	<link href="css/bootstrap.css" rel="stylesheet" />
	<!-- FONTAWESOME STYLES-->
	<link href="css/font-awesome.css" rel="stylesheet" />
	<!--CUSTOM BASIC STYLES-->
	<link href="css/basic.css" rel="stylesheet" />
	<!--CUSTOM MAIN STYLES-->
	<link href="css/custom.css" rel="stylesheet" />
	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

	<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
	
	<link href="css/ui.css" rel="stylesheet" />
	<link href="css/datepicker.css" rel="stylesheet" />	
	
	<script src="js/jquery-1.10.2.js"></script>
	
	<script type='text/javascript' src='js/jquery/jquery-ui-1.10.1.custom.min.js'></script>

	
</head>
<?php
include("php/header.php");
?>
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-head-line">Students  
					<?php
					echo (isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")?
					' <a href="invoice.php" class="btn btn-primary btn-sm pull-right">Back <i class="glyphicon glyphicon-arrow-right"></i></a>':'<a href="invoice.php?action=add" class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Add </a>';
					?>
				</h1>

				<?php

				echo $errormsg;
				?>
			</div>
		</div>



		<?php 
		if(isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")
		{
			?>

			<script type="text/javascript" src="js/validation/jquery.validate.min.js"></script>
			<div class="row">
				
				<div class="col-sm-10 col-sm-offset-1">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<?php echo ($action=="add")? "Add Student": "Edit Student"; ?>
						</div>
						<form action="invoice.php" method="post" id="signupForm1" class="form-horizontal" enctype="multipart/form-data">
							<div class="panel-body">
								<fieldset class="scheduler-border" >
									<legend  class="scheduler-border">Personal Information:</legend>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">IMAGE</label>
										<div class="col-sm-4">
											<input type="file" class="form-control" id="image" name="image" value="<?php echo $res_edit['image'];?>"  />
											<?php echo ($res_edit['image']!="" and file_exists("../upload/".$res_edit['image']))?"<img  src='../upload/".$res_edit['image']."' width='100' >":"";?>
										</div>
										<label class="col-sm-2 control-label" for="Old">REG NO* </label>
										<div class="col-sm-4">
											<?php 
												if($_REQUEST['action']=="edit")
												{
											?>
											<input type="text" class="form-control" id="regno" name="regno" value="BS-<?php echo strtoupper($prefix);?>-<?php echo $res_edit['reno'];?>" readonly />
											<?php
												}else{
											?>
											<input type="text" class="form-control" id="regno" name="regno" value="BS-<?php echo strtoupper($prefix);?>-<?php echo $res_reno['reno']+1;?>" readonly />
											<?php
										}
											?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Name* </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="sname" name="sname" value="<?php echo $res_edit['fullname'];?>"  />
										</div>
										<label class="col-sm-2 control-label" for="Old">Father Name* </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="fname" name="fname" value="<?php echo $res_edit['father'];?>"  />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Contact* </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="contact" name="contact" value="<?php echo $res_edit['mob'];?>" maxlength="10" />
										</div>
										<?php
										if($_SESSION['type']=="admin")
										{
											?>
											<label class="col-sm-2 control-label" for="Old">Branch* </label>
											<div class="col-sm-4">
												<select  class="form-control" id="branch" name="branch" >
													<option value="" >Select Branch</option>
													<?php
													$sql=exeQuery("select * from branch where status!='0' order by branch_name asc");


													while($r = fetchAssoc($sql))
													{
														echo '<option value="'.$r['id'].'"  '.(($res_edit['branch']==$r['id'])?'selected="selected"':'').'>'.$r['branch_name'].'</option>';
													}
													?>									

												</select>
											</div>
											<?php
										}elseif($_SESSION['type']=="branch"){
										?><label class="col-sm-2 control-label" for="Old">Branch* </label>
											<div class="col-sm-4">
												<select  class="form-control" id="branch" name="branch" disabled="disabled">
													<option value="" >Select Branch</option>
													<?php
													$sql=exeQuery("select * from branch where status!='0' order by branch_name asc");

													while($r = fetchAssoc($sql))
													{
														echo '<option value="'.$r['id'].'"  '.(($res_user['branch']==$r['id'])?'selected="selected"':'').'>'.$r['branch_name'].'</option>';
													}
													?>									

												</select>
											</div>
										<?php
											}
										?>
									</div>


									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">COURSES* </label>
										<div class="col-sm-4">
											<select  class="form-control" id="course_id" name="course_id" onchange="callval()">
												<option value="" >Select Course</option>
												<?php
												$sql=exeQuery("select * from courses where status!='0' order by course_name asc");
									// $q = $conn->query($sql);

												while($r = fetchAssoc($sql))
												{
													echo '<option value="'.$r['id'].'"  '.(($res_edit['courses']==$r['id'])?'selected="selected"':'').'>'.$r['course_name'].'</option>';
												}
												?>									

											</select>
										</div>
										<label class="col-sm-2 control-label" for="Old">EMAIL ID* </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="emailid" name="emailid" value="<?php echo $res_edit['emailid'];?>"  />
										</div>
									</div>
								</fieldset>

								<fieldset class="scheduler-border" >
									<legend  class="scheduler-border">Fee Information:</legend>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Total Fees* </label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="fees" name="fees" value="<?php echo $res_edit['fees'];?>"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Balance left</label>
										<div class="col-sm-10">
											<input type="text" class="form-control"  id="" name="" value="<?php echo $res_edit['balance'];?>" disabled />
										</div>
									</div>
								<?php $Advance=$res_edit['fees']-$res_edit['balance'];?>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Advance Paid Fee* </label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="advancefees" name="paid" value="<?php echo $Advance;?>" />
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Balance </label>
										<div class="col-sm-10">
											<input type="text" class="form-control"  id="balance" name="balance" value="<?php echo $res_edit['balance'];?>" disabled />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Pay Fee* </label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="" name="advancefees" value="" />
										</div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label" for="Password">Fee Remark </label>
										<div class="col-sm-10">
											<textarea class="form-control" id="remark" name="remark"><?php echo $remark;?></textarea >
										</div>
									</div>

								</fieldset>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-2">
										<input type="hidden" name="id" value="<?php echo $id;?>">
										<input type="hidden" name="hid_id" value="<?php echo $res_edit['Id'];?>">

										<button type="submit" name="save" class="btn btn-primary">Save </button>



									</div>
								</div>





							</div>
						</form>

					</div>
				</div>


			</div>




			<script type="text/javascript">


				$( document ).ready( function () {			

					$( "#joindate" ).datepicker({
						dateFormat:"yy-mm-dd",
						changeMonth: true,
						changeYear: true,
						yearRange: "1970:<?php echo date('Y');?>"
					});	



					if($("#signupForm1").length > 0)
					{

						<?php if($action=='add')
						{
							?>

							$( "#signupForm1" ).validate( {
								rules: {
									sname: "required",
									fname: "required",
									rollno: "required",
									regno: "required",
									dob: "required",
									password: "required",
									courses: "required",
									joindate: "required",
									emailid: "email",
									branch: "required",


									contact: {
										required: true,
										digits: true
									},

									fees: {
										required: true,
										digits: true
									},


									advancefees: {
										required: true,
										digits: true
									},


								},
								<?php
							}else
							{
								?>

								$( "#signupForm1" ).validate( {
									rules: {
										sname: "required",
										fname: "required",
										rollno: "number",
										regno: "required",
										password: "required",
										dob: "required",
										courses: "required",
										joindate: "required",
										emailid: "email",
										branch: "required",


										contact: {
											required: true,
											digits: true
										}

									},



									<?php
								}
								?>

								errorElement: "em",
								errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-10" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			} );

							}

						} );



							$("#fees").keyup( function(){
								$("#advancefees").val("");
								$("#balance").val(0);
								var fee = $.trim($(this).val());
								if( fee!='' && !isNaN(fee))
								{
									$("#advancefees").removeAttr("readonly");
									$("#balance").val(fee);
									$('#advancefees').rules("add", {
										max: parseInt(fee)
									});

								}
								else{
									$("#advancefees").attr("readonly","readonly");
								}

							});




							$("#advancefees").keyup( function(){

								var advancefees = parseInt($.trim($(this).val()));
								var totalfee = parseInt($("#fees").val());
								if( advancefees!='' && !isNaN(advancefees) && advancefees<=totalfee)
								{
									var balance = totalfee-advancefees;
									$("#balance").val(balance);

								}
								else{
									$("#balance").val(totalfee);
								}

							});


						</script>



						<?php
					}else{
						?>

						<link href="css/datatable/datatable.css" rel="stylesheet" />




						<div class="panel panel-default">
							<div class="panel-heading">
								Manage Student  
							</div>
							<div class="panel-body">
								<div class="table-sorting table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tSortable22">
										<thead>
											<tr>
												<th>Id
												</th>
												<th>Name/Contact</th>
												<th>Branch</th>
												<th>Course</th>
												<th>DOJ</th>
												<th>Fees</th>
												<th>Balance</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php

											if($_SESSION['type']=="branch")
											{
												$sql=exeQuery("select * from student where branch='".$res_user['branch']."' ");

											}else{
												$sql=exeQuery("select * from student");
											}
											$i=1;
											while($r = fetchAssoc($sql))
											{
												$sel_course=exeQuery("select * from ".TABLE_COURSES." where id='".$r['courses']."' ");
												$res_course=fetchAssoc($sel_course);

												$sele_branch=exeQuery("select * from ".TABLE_BRANCH." where id='".$r['branch']."' ");
												$res_branch=fetchAssoc($sele_branch);
												?>
												<td><?php echo $i;?></td>
												<td><?php echo $r['fullname'];?><br/><?php echo $r['mob'];?></td>
												<td><?php echo $res_branch['branch_name'];?></td>
												<td><?php echo $res_course['course_name'];?></td>
												<td><?php echo date("d M y", strtotime($r['joindate']));?></td>

												<td><?php echo $r['fees'];?></td>
												<td><?php echo $r['balance'];?></td>
												<td>
												<a href="invoice.php?action=edit&id=<?php echo $r['Id'];?>" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></a>

												<a onclick="return confirm(\'Are you sure you want to delete this record\');" href="invoice.php?action=delete&id=<?php echo $r['Id'];?>" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></a> 
												</td>

												</tr>
												<?php
												$i++;
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<script src="js/dataTable/jquery.dataTables.min.js"></script>

						<script>
							$(document).ready(function () {
								$('#tSortable22').dataTable({
									"bPaginate": true,
									"bLengthChange": true,
									"bFilter": true,
									"bInfo": false,
									"bAutoWidth": true });

							});


						</script>

						<?php
					}
					?>



				</div>
				<!-- /. PAGE INNER  -->
			</div>
			<!-- /. PAGE WRAPPER  -->
		</div>
		<!-- /. WRAPPER  -->

		<div id="footer-sec">
			Thebsel | Brought To You By : <a href="http://www.backstagesupporters.com" target="_blank">Backstagesupporters Pvt Ltd</a>
		</div>


		<!-- BOOTSTRAP SCRIPTS -->
		<script src="js/bootstrap.js"></script>
		<!-- METISMENU SCRIPTS -->
		<script src="js/jquery.metisMenu.js"></script>
		<!-- CUSTOM SCRIPTS -->
		<script src="js/custom1.js"></script>
		<script type="text/javascript">
			function callval(){
				var course_id = $('#course_id').val();
					$.ajax({
						url:"load_price.php",
						type:"POST",
						data:{course_id:course_id},
						success:function(data){
							$('#fees').val(data);
						}

					});
			}

			function generate(id){
				// alert(id);
				$.ajax({

						url:"generate_certificate.php",
						type:"POST",
						data:{id:id},
						success:function(data){
						 alert(data);
						}

					});
			}

			function result(id){
				$.ajax({
						url:"generate_result.php",
						type:"POST",
						data:{id:id},
						success:function(data){
							alert(data);
						}

					});
			}
		</script>
	</body>
	</html>
