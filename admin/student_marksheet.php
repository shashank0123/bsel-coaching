<?php
include("../library/raso_function.php");
check_admin();

$Total = '';
$Total2 ='';
$student_id=$_REQUEST['student_id'];

$sel_student=exeQuery("select * from ".TABLE_STUDENT." where id='".$student_id."' ");
$res_student=fetchAssoc($sel_student);


$sel_branch=exeQuery("select * from ".TABLE_BRANCH." where id='".$res_student['branch']."' ");
$res_branch=fetchAssoc($sel_branch);

$sel_course=exeQuery("select * from ".TABLE_COURSES." where id='".$res_student['courses']."' ");
$res_course=fetchAssoc($sel_course);


$sel_subject=exeQuery("select * from ".TABLE_SUBJECT." where category_id='".$res_student['courses']."' ");

 $sel_obtained1=exeQuery("select * from ".TABLE_RESULT." where student_id='$student_id' ");

foreach ($sel_obtained1 as $mark['marks_obtained'] =>$price)
               {
                $Total+=$price['marks_obtained'];
              }
			    $sel_obtained12=exeQuery("select * from ".TABLE_SUBJECT." where category_id='".$res_student['courses']."' ");
              

			foreach ($sel_obtained12 as $mark['total_marks'] =>$price)
               {
                $Total1+=$price['total_marks'];
              }

			
              $percentage=($Total/$Total1)*100;
              
              if($percentage >= 80)
              {
                $grade="A+";
                $result="Pass";
              }elseif($percentage>=70 && $percentage<80){
                $grade="B";
                $result="Pass";
              }elseif($percentage>=60 && $percentage<70){
                $grade="C";
                $result="Pass";
              }elseif($percentage>=50 && $percentage<60){
                $grade="D";
                $result="Pass";
              }elseif( $percentage<50){
                $grade="E";
                $result="Fail";
              }

              $sel_Total=exeQuery("select * from ".TABLE_SUBJECT." where category_id='".$res_student['courses']."' ");

foreach ($sel_Total as $mark['total_marks'] =>$marks)
               {
                $Total3+=$marks['total_marks'];
                 $Total2+=$marks['pass_mark'];
              }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Page</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>




  <style>
    .container{
      background-color: #ffefff;
    }
    table {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
      border: 1px solid #ddd;
    }

    th, td {
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even)/*{background-color: #f2f2f2}*/
    .acedemy{
     text-align: center ;
     
     width: 339px;
     height: 77px;
     padding: 14px;
   }
   .img-responsive{
    height: 61px;
    width: 320px;
   }
   .heading{
    margin-top: 35px;
    margin-bottom: 16px;
   }
 </style>


</head>
<body>
  <div class="container" style="border: 16px  solid green;">
   <br>
   <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-8">
     <div class="acedemy">
       <img src="images/logo.jpeg" class="img-responsive">
      </div>
 </div>
</div>

<div class="row">
  <div class="col-md-12" align="center">
    <div class="heading">
  <h4><b>An ISO 9001-2015 Certified Institution</b></h4>
   <h5>Advanced Diploma In Computer Application</h5>
 </div>
 </div>

</div>

<div class="row">
  <div class="col-md-12">

    <table class="table table-borderless responsive" align="center">
      <thead>
        <tr>
          <td>Name :</td>
          <td><?php echo $res_student['fullname'];?></td>

        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Reg.Number</td>
          <td>3405/09/ADCA/001-MUZ/2010</td>

        </tr>
        <tr>
          <td>Branch Code</td>
          <td><?php echo $res_branch['branch_code'];?></td>

        </tr>
        <tr>
          <td>Head Office</td>
          <td>Bhagwanpur</td>

        </tr>
        <tr>
          <td>Month Of Examination</td>
          <td>April 2018</td>

        </tr>
        <tr>
          <td>Certificate Number</td>
          <td>3405</td>

        </tr>
      </tbody>
    </table>
  </div>

</div>
<div class="row">
 <div class="col-md-12" style="overflow-x:auto;">
   <table class="table table-bordered responsive">
    <thead>
    	<tr>
        <th></th>
        <th></th>
        <th>Total Marks</th>
        <th>Pass Marks</th>
        <th>Marks Secured</th>
      </tr>
      <tr>
        <th>Theory</th>
        <th></th>
        <th>100</th>
        <th>40</th>
        <th>82</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>practical</td>
        <td>word</td>
        <td>50</td>
        <td>20</td>
        <td>40</td>
      </tr>
      <tr>
        <td></td>
        <td>excel</td>
        <td>50</td>
        <td>20</td>
        <td>42</td>
      </tr>

      <tr>
        <td></td>
        <td>power point</td>
        <td>50</td>
        <td>20</td>
        <td>41</td>
      </tr>
      <tr>
        <td></td>
        <td>acess</td>
        <td>50</td>
        <td>20</td>
        <td>43</td>
      </tr>
      <tr>
        <td>project</td>
        <td></td>
        <td>100</td>
        <td>42</td>
        <td>81</td>
      </tr>
      <tr>
        <td>Tally</td>
        <td></td>
        <td>100</td>
        <td>40</td>
        <td>84</td>
      </tr>
      <tr>
        <td>Total</td>
        <td></td>
        <td>500</td>
        <td>200</td>
        <td>413</td>
      </tr>
      <tr>
        <td>result</td>
        <td colspan="4" align="center">pass</td>
        
      </tr>
      <tr>
        <td>grade</td>
        <td colspan="4" align="center">A+</td>
        
      </tr>
    </tbody>
  </table>
</div>
<div class="col-md-4"><b>DATE:</b></div>
</div>
<br>
<div class="row">
	<div class="col-md-6"><b>centre Head </b></div>
	<br>
	<div class="col-md-6"> <b>Director</b></div>
</div>
<br>
<div class="row">
	<div class="col-md-12">
		<b>Topics Covered:</b>Computer Fundamentals,DOS,Windows XP,MS office 2007,MS-Access,Tally,and internet.
	</div>
	<br>
	<div class="col-md-12">
		<b>Grade Scale:</b>A+=>=80,A=> =70<80,B=>60&<=70,C=>50 &<60,D=>40&<50.
	</div>
</div>
<hr>
<div class="row">
  <div class="col-md-12" align="center">
   <h5>Head Office</h5>
   <p>Savitri Complex BhagwanpurChattirewa Road Muzaffapur-842001</p>
   <p>Contact Number:9835429138,8678044493</p>
   <p>E-mail:thebsel@gmail.com,www.thebsel.org</p>
 </div>
</div>

</div>

</body>
</html>