<?php
include("../library/raso_function.php");
check_admin();
$errormsg = '';
$action = "add";


if(isset($_POST['save']))
{
	$subject = addslashes($_POST['subject_name']);
	$total_marks = addslashes($_POST['total_marks']);
	$pass_mark = addslashes($_POST['pass_mark']);
	$course_id = addslashes($_POST['course_id']);
	$date=addslashes(date('Y-m-d'));


	$hid_id=($_POST['hid_id']!='')?addslashes($_POST['hid_id']):false;

	if($hid_id==false)
	{

		$q1=exeQuery("INSERT INTO subject set subject_name='".$subject."',total_marks='".$total_marks."',pass_mark='".$pass_mark."',category_id='".$course_id."',created_at='".$date."',created_by='".$_SESSION['id']."',status=1 " );


		echo '<script type="text/javascript">window.location="add-subject.php?act=1";</script>';

	}else
	{
		$id = addslashes($_POST['id']);	
		$sql = exeQuery("UPDATE  subject  set subject_name='".$subject."',total_marks='".$total_marks."',pass_mark='".$pass_mark."',category_id='".$course_id."',created_at='".$date."',created_by='".$_SESSION['id']."',status=1  WHERE  id  = '$id'");
		echo '<script type="text/javascript">window.location="add-subject.php?act=2";</script>';
	}


}


if(isset($_GET['action']) && $_GET['action']=="delete"){

	$delete=exeQuery("delete from subject WHERE id='".$_GET['id']."'");
	header("location: add-subject.php?act=3");

}


$action = "add";
if(isset($_GET['action']) && $_GET['action']=="edit" ){
	$id = isset($_GET['id'])?addslashes($_GET['id']):'';

	$sqlEdit = exeQuery("SELECT * FROM subject WHERE id='".$id."'");
	$res_edit=fetchAssoc($sqlEdit);
// if($sqlEdit->num_rows)
// {
// $rowsEdit = $sqlEdit->fetch_assoc();
// extract($rowsEdit);
// $action = "update";
// }else
// {
// $_GET['action']="";
// }

}


if(isset($_REQUEST['act']) && @$_REQUEST['act']=="1")
{
	$errormsg = "<div class='alert alert-success'> <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong> Student Add successfully</div>";
}else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="2")
{
	$errormsg = "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> <strong>Success!</strong> Student Edit successfully</div>";
}
else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="3")
{
	$errormsg = "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong> Student Delete successfully</div>";
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo SITENAME;?></title>

	<!-- BOOTSTRAP STYLES-->
	<link href="css/bootstrap.css" rel="stylesheet" />
	<!-- FONTAWESOME STYLES-->
	<link href="css/font-awesome.css" rel="stylesheet" />
	<!--CUSTOM BASIC STYLES-->
	<link href="css/basic.css" rel="stylesheet" />
	<!--CUSTOM MAIN STYLES-->
	<link href="css/custom.css" rel="stylesheet" />
	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	
	<link href="css/ui.css" rel="stylesheet" />
	<link href="css/datepicker.css" rel="stylesheet" />	
	
	<script src="js/jquery-1.10.2.js"></script>
	
	<script type='text/javascript' src='js/jquery/jquery-ui-1.10.1.custom.min.js'></script>

	
</head>
<?php
include("php/header.php");
?>
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-head-line">Subject  
					<?php
					echo (isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")?
					' <a href="add-subject.php" class="btn btn-primary btn-sm pull-right">Back <i class="glyphicon glyphicon-arrow-right"></i></a>':'<a href="add-subject.php?action=add" class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Add </a>';
					?>
				</h1>

				<?php

				echo $errormsg;
				?>
			</div>
		</div>



		<?php 
		if(isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")
		{
			?>

			<script type="text/javascript" src="js/validation/jquery.validate.min.js"></script>
			<div class="row">
				
				<div class="col-sm-10 col-sm-offset-1">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<?php echo ($action=="add")? "Add Subject": "Edit Subject"; ?>
						</div>
						<form action="add-subject.php" method="post" id="signupForm1" class="form-horizontal">
							<div class="panel-body">
								<fieldset class="scheduler-border" >
									<legend  class="scheduler-border">Personal Information:</legend>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Course* </label>
										<div class="col-sm-4">
											<select  class="form-control" id="course_id" name="course_id" onchange="callval()">
												<option value="" >Select Course</option>
												<?php
												$sql=exeQuery("select * from courses where status!='0' order by course_name asc");
												while($r = fetchAssoc($sql))
												{
													
													echo '<option value="'.$r['id'].'"  '.(($res_edit['category_id']==$r['id'])?'selected="selected"':'').'>'.$r['course_name'].'</option>';
												}
												?>									

											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Name* </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="subject_name" name="subject_name" value="<?php echo $res_edit['subject_name'];?>"  />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Total Marks* </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="total_marks" name="total_marks" value="<?php echo $res_edit['total_marks'];?>"  />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="Old">Passing Marks* </label>
										<div class="col-sm-4">
											<input type="text" class="form-control" id="pass_mark" name="pass_mark" value="<?php echo $res_edit['pass_mark'];?>"  />
										</div>
									</div>
								</div>
							</fieldset>
							<div class="form-group">
								<div class="col-sm-8 col-sm-offset-2">
									<input type="hidden" name="id" value="<?php echo $id;?>">
									<input type="hidden" name="hid_id" value="<?php echo $res_edit['id'];?>">

									<button type="submit" name="save" class="btn btn-primary">Save </button>



								</div>
							</div>





						</div>
					</form>

				</div>
			</div>

			
		</div>




		<script type="text/javascript">


			$( document ).ready( function () {			

				$( "#joindate" ).datepicker({
					dateFormat:"yy-mm-dd",
					changeMonth: true,
					changeYear: true,
					yearRange: "1970:<?php echo date('Y');?>"
				});	



				if($("#signupForm1").length > 0)
				{

					<?php if($action=='add')
					{
						?>

						$( "#signupForm1" ).validate( {
							rules: {
								sname: "required",
								fname: "required",
								rollno: "required",
								regno: "required",
								dob: "required",
								password: "required",
								courses: "required",
								joindate: "required",
								emailid: "email",
								branch: "required",


								contact: {
									required: true,
									digits: true
								},

								fees: {
									required: true,
									digits: true
								},


								advancefees: {
									required: true,
									digits: true
								},


							},
							<?php
						}else
						{
							?>

							$( "#signupForm1" ).validate( {
								rules: {
									sname: "required",
									fname: "required",
									rollno: "number",
									regno: "required",
									password: "required",
									dob: "required",
									courses: "required",
									joindate: "required",
									emailid: "email",
									branch: "required",


									contact: {
										required: true,
										digits: true
									}

								},



								<?php
							}
							?>

							errorElement: "em",
							errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-10" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			} );

						}

					} );



						$("#fees").keyup( function(){
							$("#advancefees").val("");
							$("#balance").val(0);
							var fee = $.trim($(this).val());
							if( fee!='' && !isNaN(fee))
							{
								$("#advancefees").removeAttr("readonly");
								$("#balance").val(fee);
								$('#advancefees').rules("add", {
									max: parseInt(fee)
								});

							}
							else{
								$("#advancefees").attr("readonly","readonly");
							}

						});




						$("#advancefees").keyup( function(){

							var advancefees = parseInt($.trim($(this).val()));
							var totalfee = parseInt($("#fees").val());
							if( advancefees!='' && !isNaN(advancefees) && advancefees<=totalfee)
							{
								var balance = totalfee-advancefees;
								$("#balance").val(balance);

							}
							else{
								$("#balance").val(totalfee);
							}

						});


					</script>



					<?php
				}else{
					?>

					<link href="css/datatable/datatable.css" rel="stylesheet" />

					<div class="panel panel-default">
						<div class="panel-heading">
							Manage Subject  
						</div>
						<div class="panel-body">
							<div class="table-sorting table-responsive">
								<table class="table table-striped table-bordered table-hover" id="tSortable22">
									<thead>
										<tr>
											<th>Id</th>
											<th>Course Name</th>
											<th>Subject</th>
											<th>Total Marks</th>
											<th>Passing Marks</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$sql=exeQuery("select * from subject ");

										$i=1;
										while($r = fetchAssoc($sql))
										{
											$sel_course=exeQuery("select * from courses where id='".$r['category_id']."' ");
											$res_course=fetchAssoc($sel_course);
											echo '<tr>
											<td>'.$i.'</td>
											<td>'.$res_course['course_name'].'</td>
											<td>'.$r['subject_name'].'</td>
											<td>'.$r['total_marks'].'</td>
											<td>'.$r['pass_mark'].'</td>
											<td>
											<a href="add-subject.php?action=edit&id='.$r['id'].'" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></a>
											
											<a onclick="return confirm(\'Are you sure you want to delete this record\');" href="add-subject.php?action=delete&id='.$r['id'].'" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></a> </td>
											
											</tr>';
											$i++;
										}
										?>



									</tbody>
								</table>
							</div>
						</div>
					</div>

					<script src="js/dataTable/jquery.dataTables.min.js"></script>

					<script>
						$(document).ready(function () {
							$('#tSortable22').dataTable({
								"bPaginate": true,
								"bLengthChange": true,
								"bFilter": true,
								"bInfo": false,
								"bAutoWidth": true });

						});


					</script>

					<?php
				}
				?>
				
				

			</div>
			<!-- /. PAGE INNER  -->
		</div>
		<!-- /. PAGE WRAPPER  -->
	</div>
	<!-- /. WRAPPER  -->

	<div id="footer-sec">
		Thebsel | Brought To You By : <a href="http://www.backstagesupporters.com" target="_blank">Backstagesupporters Pvt Ltd</a>
	</div>


	<!-- BOOTSTRAP SCRIPTS -->
	<script src="js/bootstrap.js"></script>
	<!-- METISMENU SCRIPTS -->
	<script src="js/jquery.metisMenu.js"></script>
	<!-- CUSTOM SCRIPTS -->
	<script src="js/custom1.js"></script>


</body>
</html>


