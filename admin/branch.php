<?php
include("../library/raso_function.php");
check_admin();

$errormsg = '';
$action = "add";

$branch='';
$address='';
$detail = '';
$id= '';

if(isset($_POST['save']))
{

	$branch = addslashes($_POST['branch']);
	$branch_code = addslashes($_POST['branch_code']);
	$email = addslashes($_POST['email']);
	$mobile = addslashes($_POST['mobile']);
	$address = addslashes($_POST['address']);
	$detail = addslashes($_POST['detail']);

	$hid_id=($_POST['hid_id']!='')?addslashes($_POST['hid_id']):false;

	if($hid_id==false)
	{

		$sql = exeQuery("insert into ".TABLE_BRANCH." set branch_name='".$branch."',branch_code='".$branch_code."',address='".$address."',email='".$email."',mobile='".$mobile."',details='".$detail."',created_by='".$_SESSION['id']."',created_at='".date('Y-m-d')."',status=1 ");


		echo '<script type="text/javascript">window.location="branch.php?act=1";</script>';

	}else
	{
		$id = addslashes($_POST['id']);	
		// $sql = $conn->query("UPDATE  branch  SET  branch  = '$branch', address  = '$address', detail  = '$detail'  WHERE  id  = '$id'");

		$sql=exeQuery("UPDATE  branch  set branch_name='".$branch."',branch_code='".$branch_code."',address='".$address."',email='".$email."',mobile='".$mobile."',details='".$detail."',created_by='".$_SESSION['id']."',created_at='".date('Y-m-d')."',status=1  WHERE  id  = '$id'");
		echo '<script type="text/javascript">window.location="branch.php?act=2";</script>';
	}



}




if(isset($_GET['action']) && $_GET['action']=="delete"){

	// $conn->query("UPDATE  branch set delete_status = '1'  WHERE id='".$_GET['id']."'");
	$delete=exeQuery("UPDATE  branch set status = '0'  WHERE id='".$_GET['id']."'");
	header("location: branch.php?act=3");

}


$action = "id";
if(isset($_GET['id']) && $_GET['id']!="" ){
	$id = isset($_GET['id'])?addslashes($_GET['id']):'';

	// $sqlEdit = $conn->query("SELECT * FROM branch WHERE id='".$id."'");
	$sqlEdit=exeQuery("SELECT * FROM branch WHERE id='".$id."'");
	
	if($sqlEdit->num_rows)
	{
		$res_edit=fetchAssoc($sqlEdit);
		$action = "update";
	}else
	{
		$_GET['action']="";
	}

}


if(isset($_REQUEST['act']) && @$_REQUEST['act']=="1")
{
	$errormsg = "<div class='alert alert-success'><strong>Success!</strong> Branch Add successfully</div>";
}else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="2")
{
	$errormsg = "<div class='alert alert-success'><strong>Success!</strong> Branch Edit successfully</div>";
}
else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="3")
{
	$errormsg = "<div class='alert alert-success'><strong>Success!</strong> Branch Delete successfully</div>";
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo SITENAME;?></title>

	<!-- BOOTSTRAP STYLES-->
	<link href="css/bootstrap.css" rel="stylesheet" />
	<!-- FONTAWESOME STYLES-->
	<link href="css/font-awesome.css" rel="stylesheet" />
	<!--CUSTOM BASIC STYLES-->
	<link href="css/basic.css" rel="stylesheet" />
	<!--CUSTOM MAIN STYLES-->
	<link href="css/custom.css" rel="stylesheet" />
	


	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	
	<script src="js/jquery-1.10.2.js"></script>


	
</head>
<?php
include("php/header.php");
?>
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-head-line">Branch  
					<?php
					echo (isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")?
					' <a href="branch.php" class="btn btn-primary btn-sm pull-right">Back <i class="glyphicon glyphicon-arrow-right"></i></a>':'<a href="branch.php?action=add" class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Add </a>';
					?>
				</h1>

				<?php

				echo $errormsg;
				?>
			</div>
		</div>



		<?php 
		if(isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")
		{
			?>

			<script type="text/javascript" src="js/validation/jquery.validate.min.js"></script>
			<div class="row">
				
				<div class="col-sm-8 col-sm-offset-2">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<?php echo ($action=="add")? "Add Branch": "Edit Branch"; ?>
						</div>
						<form action="branch.php" method="post" id="signupForm1" class="form-horizontal">
							<div class="panel-body">




								<div class="form-group">
									<label class="col-sm-2 control-label" for="Old"> Name </label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="branch" name="branch" value="<?php echo $res_edit['branch_name'];?>"  />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Old"> Branch Code </label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="branch_code" name="branch_code" value="<?php echo $res_edit['branch_code'];?>"  />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Old">Email </label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="email" name="email" value="<?php echo $res_edit['email'];?>"  />
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label" for="Old">Mobile </label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $res_edit['mobile'];?>"  />
									</div>
								</div>


								<div class="form-group">
									<label class="col-sm-2 control-label" for="Password"> Address </label>
									<div class="col-sm-10">
										<textarea class="form-control" id="address" name="address"><?php echo $res_edit['address'];?></textarea >
									</div>
								</div>


								<div class="form-group">
									<label class="col-sm-2 control-label" for="Confirm"> Detail</label>
									<div class="col-sm-10">
										<textarea class="form-control" name="detail" id="detail"><?php echo $res_edit['details'];?></textarea >
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-2">
										<input type="hidden" name="id" value="<?php echo $id;?>">
										<input type="hidden" name="hid_id" value="<?php echo $res_edit['id'];?>">

										<button type="submit" name="save" class="btn btn-primary">Save </button>
									</div>
								</div>





							</div>
						</form>

					</div>
				</div>


			</div>




			<script type="text/javascript">


				$( document ).ready( function () {			

					if($("#signupForm1").length > 0)
					{
						$( "#signupForm1" ).validate( {
							rules: {
								branch: "required",
								address: "required"



							},
							messages: {
								branch: "Please enter branch name",
								address: "Please enter address"


							},
							errorElement: "em",
							errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-10" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			} );

					}

				} );
			</script>



			<?php
		}else{
			?>

			<link href="css/datatable/datatable.css" rel="stylesheet" />




			<div class="panel panel-default">
				<div class="panel-heading">
					Manage Branch  
				</div>
				<div class="panel-body">
					<div class="table-sorting table-responsive">

						<table class="table table-striped table-bordered table-hover" id="tSortable22">
							<thead>
								<tr>
									<th>#</th>
									<th>Branch</th>
									<th>Branch Code</th>
									<th>Address</th>
									<th>Email</th>
									<th>Mobile</th>
									<th>Detail</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									// $sql = "select * from branch where delete_status='0'";
								$sql=exeQuery("select * from branch where status='1'");
								$i=1;
								while($r = fetchAssoc($sql))
								{
									echo '<tr>
									<td>'.$i.'</td>
									<td>'.$r['branch_name'].'</td>
									<td>'.$r['branch_code'].'</td>
									<td>'.$r['address'].'</td>
									<td>'.$r['email'].'</td>
									<td>'.$r['mobile'].'</td>
									<td>'.$r['details'].'</td>
									<td>
									<a href="branch.php?action=edit&id='.$r['id'].'" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></a>

									<a onclick="return confirm(\'Are you sure you want to delete this record\');" href="branch.php?action=delete&id='.$r['id'].'" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></a> </td>
									</tr>';
									$i++;
								}
								?>



							</tbody>
						</table>
					</div>
				</div>
			</div>

			<script src="js/dataTable/jquery.dataTables.min.js"></script>
			<script>
				$(document).ready(function () {
					$('#tSortable22').dataTable({
						"bPaginate": true,
						"bLengthChange": false,
						"bFilter": true,
						"bInfo": false,
						"bAutoWidth": true });

				});


			</script>

			<?php
		}
		?>



	</div>
	<!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
	Thebsel | Brought To You By : <a href="http://www.backstagesupporters.com" target="_blank">Backstagesupporters Pvt Ltd</a>
</div>


<!-- BOOTSTRAP SCRIPTS -->
<script src="js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/custom1.js"></script>


</body>
</html>
