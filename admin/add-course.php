<?php
include("../library/raso_function.php");
check_admin();

$errormsg = '';
$action = "add";

$branch='';
$address='';
$detail = '';
$id= '';

if(isset($_POST['save']))
{
 
	$course_name = addslashes($_POST['course_name']);
	$description = addslashes($_POST['description']);
	$seats = addslashes($_POST['seats']);
	$duration = addslashes($_POST['duration']);
	$start_date = addslashes($_POST['start_date']);
	$created_at=addslashes(date('Y-m-d'));
	$fees=addslashes($_POST['fees']);
	$subject_id = implode(',', $_POST['subject_id']); 
	

	
	

	$hid_id=($_POST['hid_id']!='')?addslashes($_POST['hid_id']):false;

	if(@$_FILES['image']['tmp_name']!="")
	{
		$image_name=time().$_FILES['image']['name'];
		if($_FILES['image']['type']=="image/png" or $_FILES['image']['type']=="image/jpeg" or $_FILES['image']['type']=="image/gif")
		{
			$file_move=move_uploaded_file($_FILES['image']['tmp_name'],"../upload/".$image_name);
			if(!$file_move)
			{
				$_SESSION['msg']="Image failed to upload!!!";
				$_SESSION['msg_type']="alert-danger";
				header("Location:".$_SERVER[REQUEST_URI]);
				exit();
			}
			else
			{
				$set.=" , image='".$image_name."'";
			}
		}
		else
		{
			$_SESSION['msg']="Please choose .jpg, . png, .gif to upload!!!";
			$_SESSION['msg_type']="alert-danger";
			header("Location:".$_SERVER[REQUEST_URI]);
			exit();
		}
	}
	elseif($hid_id==false)
	{
		$_SESSION['msg']="Please choose .jpg, . png, .gif to upload!!!";
		$_SESSION['msg_type']="alert-danger";
		header("Location:".$_SERVER[REQUEST_URI]);
		exit();
	}

	if($hid_id==false)
	{

		$sql = exeQuery("insert into ".TABLE_COURSES." set course_name='".$course_name."',course_desc='".$description."',seats='".$seats."',duration='".$duration."',starting_date='".$start_date."',created_by='".$_SESSION['id']."',fees='".$fees."',created_at='".$created_at."',status=1, subject_id='".$subject_id."'$set ");


		echo '<script type="text/javascript">window.location="add-course.php?act=1";</script>';

	}else
	{
		$id = addslashes($_POST['id']);

		if($file_move)
		{
			$qry_event_image=exeQuery("select * from ".TABLE_COURSES." where id='".$hid_id."'");
			$res_image=fetchAssoc($qry_event_image);
			if($res_image['image']!="")
			{
				unlink("../upload/".$res_image['image']);
			}
		}

		$sql=exeQuery("UPDATE  ".TABLE_COURSES."  set course_name='".$course_name."',course_desc='".$description."',seats='".$seats."',duration='".$duration."',starting_date='".$start_date."',created_by='".$_SESSION['id']."',duration='".$duration."',fees='".$fees."',created_at='".$created_at."',status=1,subject_id='".$subject_id."' $set  WHERE  id  = '$id'");
		echo '<script type="text/javascript">window.location="add-course.php?act=2";</script>';
	}



}




if(isset($_GET['action']) && $_GET['action']=="delete"){

	// $conn->query("UPDATE  branch set delete_status = '1'  WHERE id='".$_GET['id']."'");
	$delete=exeQuery("delete from ".TABLE_COURSES."  WHERE id='".$_GET['id']."'");

	header("location: add-course.php?act=3");

}


$action = "id";
if(isset($_GET['id']) && $_GET['id']!="" ){
	$id = isset($_GET['id'])?addslashes($_GET['id']):'';

	// $sqlEdit = $conn->query("SELECT * FROM branch WHERE id='".$id."'");
	$sqlEdit=exeQuery("SELECT * FROM ".TABLE_COURSES." WHERE id='".$id."'");
	
	if($sqlEdit->num_rows)
	{
		$res_edit=fetchAssoc($sqlEdit); 
		$action = "update";
	}else
	{
		$_GET['action']="";
	}

}


if(isset($_REQUEST['act']) && @$_REQUEST['act']=="1")
{
	$errormsg = "<div class='alert alert-success'><strong>Success!</strong> Branch Add successfully</div>";
}else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="2")
{
	$errormsg = "<div class='alert alert-success'><strong>Success!</strong> Branch Edit successfully</div>";
}
else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="3")
{
	$errormsg = "<div class='alert alert-success'><strong>Success!</strong> Branch Delete successfully</div>";
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo SITENAME;?></title>

	<!-- BOOTSTRAP STYLES-->
	<link href="css/bootstrap.css" rel="stylesheet" />
	<!-- FONTAWESOME STYLES-->
	<link href="css/font-awesome.css" rel="stylesheet" />
	<!--CUSTOM BASIC STYLES-->
	<link href="css/basic.css" rel="stylesheet" />
	<!--CUSTOM MAIN STYLES-->
	<link href="css/custom.css" rel="stylesheet" />
	


	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	
	<script src="js/jquery-1.10.2.js"></script>


	
</head>
<?php
include("php/header.php");
?>
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-head-line">Courses  
					<?php
					echo (isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")?
					' <a href="add-course.php" class="btn btn-primary btn-sm pull-right">Back <i class="glyphicon glyphicon-arrow-right"></i></a>':'<a href="add-course.php?action=add" class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Add </a>';
					?>
				</h1>

				<?php

				echo $errormsg;
				?>
			</div>
		</div>



		<?php 
		if(isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")
		{
			?>

			<script type="text/javascript" src="js/validation/jquery.validate.min.js"></script>
			<div class="row">
				
				<div class="col-sm-8 col-sm-offset-2">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<?php echo ($action=="add")? "Edit Course": "Add Course"; ?>
						</div>
						<form action="add-course.php" method="post" id="signupForm1" class="form-horizontal" enctype="multipart/form-data">
							<div class="panel-body">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Old">Course Name </label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="course_name" name="course_name" value="<?php echo $res_edit['course_name'];?>"  />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Old">Fees</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="fees" name="fees" value="<?php echo $res_edit['fees'];?>" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Old">Image </label>
									<div class="col-sm-10">
										<input type="file" class="form-control" id="image" name="image" value="<?php echo $res_edit['image'];?>"  />
										<?php echo ($res_edit['image']!="" and file_exists("../upload/".$res_edit['image']))?"<img  src='../upload/".$res_edit['image']."' width='100' >":"";?>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label" for="Password"> description </label>
									<div class="col-sm-10">
										<textarea class="form-control" id="description" name="description"><?php echo $res_edit['course_desc'];?></textarea >
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Old">No of seats </label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="seats" name="seats" value="<?php echo $res_edit['seats'];?>"  />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Old">Duration</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="duration" name="duration" value="<?php echo $res_edit['duration'];?>"  />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Confirm">Starting Date</label>
									<div class="col-sm-10">
										<input type="date" class="form-control" id="start_date" name="start_date" value="<?php echo $res_edit['starting_date'];?>"  />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Confirm">Subjects</label>
									<div class="col-sm-10"><?php $subject_ids = explode(',', $res_edit['subject_id']); ;?>
										<select name="subject_id[]" multiple style="width: 100%">
											 <option value="" disabled="disabled">Choose Subject</option>
											<?php 
											
											$sel_subject=exeQuery("select * from subject");
																				
											while($res_subject=fetchAssoc($sel_subject))
											{	$selected = '';
												if(in_array($res_subject['id'], $subject_ids)){
													$selected = 'selected = "selected"';
												}																		
											?>
											<option value="<?php echo $res_subject['id'];?>" <?php echo $selected;?>><?php echo $res_subject['subject_name'];?></option>
											<?php
											}
											?>
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-2">
										<input type="hidden" name="id" value="<?php echo $id;?>">
										<input type="hidden" name="hid_id" value="<?php echo $res_edit['id'];?>">

										<button type="submit" name="save" class="btn btn-primary">Save </button>
									</div>
								</div>





							</div>
						</form>

					</div>
				</div>


			</div>




			<script type="text/javascript">


				$( document ).ready( function () {			

					if($("#signupForm1").length > 0)
					{
						$( "#signupForm1" ).validate( {
							rules: {
								branch: "required",
								address: "required"



							},
							messages: {
								branch: "Please enter branch name",
								address: "Please enter address"


							},
							errorElement: "em",
							errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-10" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			} );

					}

				} );
			</script>



			<?php
		}else{
			?>

			<link href="css/datatable/datatable.css" rel="stylesheet" />




			<div class="panel panel-default">
				<div class="panel-heading">
					Manage Course  
				</div>
				<div class="panel-body">
					<div class="table-sorting table-responsive">

						<table class="table table-striped table-bordered table-hover" id="tSortable22">
							<thead>
								<tr>
									<th>#</th>
									<th>Course Name</th>
									<th>Fees</th>
									<th>Image</th>
									<th>Starting date</th>
									<th>Duration</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									// $sql = "select * from branch where delete_status='0'";
								$sql=exeQuery("select * from ".TABLE_COURSES." where status='1'");
								$i=1;
								while($r = fetchAssoc($sql))
								{
									?>
									<tr>
										<td><?php echo $i;?></td>
										<td><?php echo $r['course_name'];?></td>
										<td><?php echo $r['fees'];?></td>
										<td><?php echo ($r['image']!="" and file_exists("../upload/".$r['image']))?"<img  src='../upload/".$r['image']."' width='100' >":"NO IMAGE";?></td>
										<td><?php echo $r['starting_date'];?></td>
										<td><?php echo $r['duration'];?></td>
										<td>
											<a href="add-course.php?action=edit&id=<?php echo $r['id'];?>" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></a>

											<a onclick="return confirm(\'Are you sure you want to delete this record\');" href="add-course.php?action=delete&id=<?php echo $r['id'];?>" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></a> </td>
										</tr>
										<?php
										$i++;
									}
									?>



								</tbody>
							</table>
						</div>
					</div>
				</div>

				<script src="js/dataTable/jquery.dataTables.min.js"></script>
				<script>
					$(document).ready(function () {
						$('#tSortable22').dataTable({
							"bPaginate": true,
							"bLengthChange": false,
							"bFilter": true,
							"bInfo": false,
							"bAutoWidth": true });

					});


				</script>

				<?php
			}
			?>



		</div>
		<!-- /. PAGE INNER  -->
	</div>
	<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
	Thebsel | Brought To You By : <a href="http://www.backstagesupporters.com" target="_blank">Backstagesupporters Pvt Ltd</a>
</div>


<!-- BOOTSTRAP SCRIPTS -->
<script src="js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/custom1.js"></script>


</body>
</html>
