<?php
include("../library/raso_function.php");
check_admin();

$errormsg = '';
$action = "add";

$branch='';
$address='';
$detail = '';
$id= '';
$sel_user=exeQuery("select * from admin where id='".$_SESSION['id']."' ");
$res_user=fetchAssoc($sel_user);

$sel_prefix=exeQuery("select * from branch where id='".$res_user['branch']."' ");
$res_prefix=fetchAssoc($sel_prefix);


if(isset($_POST['save']))
{

	$course_id = addslashes($_POST['course_id']);
	$branch_id = addslashes($res_user['branch']);


	$created_at=addslashes(date('Y-m-d'));

	$hid_id=($_POST['hid_id']!='')?addslashes($_POST['hid_id']):false;

	if(@$_FILES['doc']['size']!="")
	{

		$doc=time().$_FILES['doc']['name'];

		if($_FILES['doc']['type']=="application/pdf")
		{

		$file_move=move_uploaded_file($_FILES['doc']['tmp_name'],"../upload/".$doc);

			if(!$file_move)
			{
				$_SESSION['msg']="Image failed to upload!!!";
				$_SESSION['msg_type']="alert-danger";
				header("Location:".$_SERVER[REQUEST_URI]);
				exit();
			}
			else
			{
				$set.=" , doc='".$doc."'";
			}
		}
		else
		{
			$_SESSION['msg']="Please choose .jpg, . png, .gif to upload!!!";
			$_SESSION['msg_type']="alert-danger";
			header("Location:".$_SERVER[REQUEST_URI]);
			exit();
		}
	}
	elseif($hid_id==false)
	{
		$_SESSION['msg']="Please choose .pdf to upload!!!";
		$_SESSION['msg_type']="alert-danger";
		header("Location:".$_SERVER[REQUEST_URI]);
		exit();
	}

	if($hid_id==false)
	{

		$sql = exeQuery("insert into ".TABLE_HOMEWORK." set course_id='".$course_id."',branch_id='".$branch_id."',created_by='".$_SESSION['id']."',created_at='".$created_at."',status=1 $set");

		echo '<script type="text/javascript">window.location="add-homework.php?act=1";</script>';

	}else
	{
		$id = addslashes($_POST['id']);


		$sql=exeQuery("UPDATE  ".TABLE_HOMEWORK."  set course_id='".$course_id."',branch_id='".$branch_id."',created_by='".$_SESSION['id']."',created_at='".$created_at."',status=1 $set  WHERE  id  = '$id'");
		echo '<script type="text/javascript">window.location="add-homework.php?act=2";</script>';
	}



}

if(isset($_GET['action']) && $_GET['action']=="delete"){

	// $conn->query("UPDATE  branch set delete_status = '1'  WHERE id='".$_GET['id']."'");
	$delete=exeQuery("delete from ".TABLE_HOMEWORK."  WHERE id='".$_GET['id']."'");

	header("location: add-homework.php?act=3");

}


$action = "id";
if(isset($_GET['id']) && $_GET['id']!="" ){
	$id = isset($_GET['id'])?addslashes($_GET['id']):'';

	// $sqlEdit = $conn->query("SELECT * FROM branch WHERE id='".$id."'");
	$sqlEdit=exeQuery("SELECT * FROM ".TABLE_HOMEWORK." WHERE id='".$id."'");
	
	if($sqlEdit->num_rows)
	{
		$res_edit=fetchAssoc($sqlEdit);
		$action = "update";
	}else
	{
		$_GET['action']="";
	}

}


if(isset($_REQUEST['act']) && @$_REQUEST['act']=="1")
{
	$errormsg = "<div class='alert alert-success'><strong>Success!</strong> Homework Add successfully</div>";
}else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="2")
{
	$errormsg = "<div class='alert alert-success'><strong>Success!</strong> Homework Edit successfully</div>";
}
else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="3")
{
	$errormsg = "<div class='alert alert-success'><strong>Success!</strong> Homework Delete successfully</div>";
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo SITENAME;?></title>

	<!-- BOOTSTRAP STYLES-->
	<link href="css/bootstrap.css" rel="stylesheet" />
	<!-- FONTAWESOME STYLES-->
	<link href="css/font-awesome.css" rel="stylesheet" />
	<!--CUSTOM BASIC STYLES-->
	<link href="css/basic.css" rel="stylesheet" />
	<!--CUSTOM MAIN STYLES-->
	<link href="css/custom.css" rel="stylesheet" />
	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	
	<script src="js/jquery-1.10.2.js"></script>
</head>
<?php
include("php/header.php");
?>
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-head-line">Homework  
					<?php
					echo (isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")?
					' <a href="add-homework.php" class="btn btn-primary btn-sm pull-right">Back <i class="glyphicon glyphicon-arrow-right"></i></a>':'<a href="add-homework.php?action=add" class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Add </a>';
					?>
				</h1>

				<?php

				echo $errormsg;
				?>
			</div>
		</div>
		<?php 
		if(isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")
		{
			?>
			<script type="text/javascript" src="js/validation/jquery.validate.min.js"></script>
			<div class="row">
				
				<div class="col-sm-8 col-sm-offset-2">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<?php echo ($action=="add")? "Edit Notice": "Add Notice"; ?>
						</div>
						<form action="add-homework.php" method="post" id="signupForm1" class="form-horizontal" enctype="multipart/form-data">
							<div class="panel-body">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Password">Course*</label>
									<div class="col-sm-10">
										<select  class="form-control" id="course_id" name="course_id" onchange="callval()">
											<option value="" >Select Course</option>
											<?php
											$sql=exeQuery("select * from courses where status!='0' order by course_name asc");
									// $q = $conn->query($sql);

											while($r = fetchAssoc($sql))
											{
												echo '<option value="'.$r['id'].'"  '.(($res_edit['course_id']==$r['id'])?'selected="selected"':'').'>'.$r['course_name'].'</option>';
											}
											?>									

										</select>
									</div>
								</div>
								<div class="form-group">
									<?php
										if($_SESSION['type']=="admin")
										{
											?>
											<label class="col-sm-2 control-label" for="Old">Branch* </label>
											<div class="col-sm-4">
												<select  class="form-control" id="branch_id" name="branch_id" onchange="changecode('<?php echo $res_user['id'];?>')" >
													<option value="" >Select Branch</option>
													<?php
													$sql=exeQuery("select * from branch where status!='0' order by branch_name asc");


													while($r = fetchAssoc($sql))
													{
														echo '<option value="'.$r['id'].'"  '.(($res_edit['branch']==$r['id'])?'selected="selected"':'').'>'.$r['branch_name'].'</option>';
													}
													?>									

												</select>
											</div>
											<?php
										}elseif($_SESSION['type']=="branch"){
											?><label class="col-sm-2 control-label" for="Old">Branch </label>
											<div class="col-sm-4">
												<select  class="form-control" id="branch_id" name="branch_id" disabled="disabled">
													<option value="" >Select Branch</option>
													<?php
													$sql=exeQuery("select * from branch where status!='0' order by branch_name asc");

													while($r = fetchAssoc($sql))
													{
														echo '<option value="'.$r['id'].'"  '.(($res_user['branch']==$r['id'])?'selected="selected"':'').'>'.$r['branch_name'].'</option>';
													}
													?>									

												</select>
											</div>
											<?php
										}
										?>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="Password">Upload Homework</label>
									<div class="col-sm-10">
										<input type="file" class="form-control" id="doc" name="doc" value="<?php echo $res_edit['doc'];?>"  />
										<a href="<?php echo ($res_edit['doc']!="" and file_exists("../upload/".$res_edit['doc']))?" ../upload/".$res_edit['doc']." ":"<a href='#' >NO doc</a>";?>" ><i class="fa fa-file-pdf-o" style="font-size:48px;color:red"></i></a>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-2">
										<input type="hidden" name="id" value="<?php echo $id;?>">
										<input type="hidden" name="hid_id" value="<?php echo $res_edit['id'];?>">

										<button type="submit" name="save" class="btn btn-primary">Publish</button>
									</div>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>
			<script type="text/javascript">
				$( document ).ready( function () {			

					if($("#signupForm1").length > 0)
					{
						$( "#signupForm1" ).validate( {
							rules: {
								branch: "required",
								address: "required"
							},
							messages: {
								branch: "Please enter branch name",
								address: "Please enter address"
							},
							errorElement: "em",
							errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					element.parents( ".col-sm-10" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			} );

					}

				} );
			</script>
			<?php
		}else{
			?>
			<link href="css/datatable/datatable.css" rel="stylesheet" />
			<div class="panel panel-default">
				<div class="panel-heading">
					Manage Notice  
				</div>
				<div class="panel-body">
					<div class="table-sorting table-responsive">

						<table class="table table-striped table-bordered table-hover" id="tSortable22">
							<thead>
								<tr>
									<th>#</th>
									<th>Notice</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($_SESSION['type']=='admin'){
									$sql=exeQuery("select * from ".TABLE_HOMEWORK." where status='1'");
								}else{
									
								$sql=exeQuery("select * from ".TABLE_HOMEWORK." where status='1' and branch_id='".$res_prefix['id']."'");
								}
								$i=1;
								while($r = fetchAssoc($sql))
								{

									?>
									<td><?php echo $i;?></td>

									<td><a href="<?php echo ($r['doc']!="" and file_exists("../upload/".$r['doc']))?" ../upload/".$r['doc']." ":"<a href='#' >NO doc</a>";?>" ><i class="fa fa-file-pdf-o" style="font-size:48px;color:red"></i></a>
									</td>
									<td>
									
									<a href="add-homework.php?action=edit&id=<?php echo $r['id'];?>" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></a>
									
									<a onclick="return confirm('Are you sure you want to delete this record');" href="add-homework.php?action=delete&id=<?php echo $r['id'];?>" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span></a> </td>
									
									</tr>
									<?php
									$i++;
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<script src="js/dataTable/jquery.dataTables.min.js"></script>
			<script>
				$(document).ready(function () {
					$('#tSortable22').dataTable({
						"bPaginate": true,
						"bLengthChange": false,
						"bFilter": true,
						"bInfo": false,
						"bAutoWidth": true });

				});
			</script>

			<?php
		}
		?>
	</div>
	<!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->

<div id="footer-sec">
	Thebsel | Brought To You By : <a href="http://www.backstagesupporters.com" target="_blank">Backstagesupporters Pvt Ltd</a>
</div>


<!-- BOOTSTRAP SCRIPTS -->
<script src="js/bootstrap.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="js/custom1.js"></script>


</body>
</html>
