<?php
include("../library/raso_function.php");
check_admin();

$Total = '';
$Total2 ='';
$student_id=$_REQUEST['student_id'];

$sel_student=exeQuery("select * from ".TABLE_STUDENT." where id='".$student_id."' ");
$res_student=fetchAssoc($sel_student);


$sel_branch=exeQuery("select * from ".TABLE_BRANCH." where id='".$res_student['branch']."' ");
$res_branch=fetchAssoc($sel_branch);

$sel_course=exeQuery("select * from ".TABLE_COURSES." where id='".$res_student['courses']."' ");
$res_course=fetchAssoc($sel_course);


$sel_subject=exeQuery("select * from ".TABLE_SUBJECT." where category_id='".$res_student['courses']."' ");

 $sel_obtained1=exeQuery("select * from ".TABLE_RESULT." where student_id='$student_id' ");

foreach ($sel_obtained1 as $mark['marks_obtained'] =>$price)
               {
                $Total+=$price['marks_obtained'];
              }
			    $sel_obtained12=exeQuery("select * from ".TABLE_SUBJECT." where category_id='".$res_student['courses']."' ");
              

			foreach ($sel_obtained12 as $mark['total_marks'] =>$price)
               {
                $Total1+=$price['total_marks'];
              }

			
              $percentage=($Total/$Total1)*100;
              
              if($percentage >= 80)
              {
                $grade="A+";
                $result="Pass";
              }elseif($percentage>=70 && $percentage<80){
                $grade="B";
                $result="Pass";
              }elseif($percentage>=60 && $percentage<70){
                $grade="C";
                $result="Pass";
              }elseif($percentage>=50 && $percentage<60){
                $grade="D";
                $result="Pass";
              }elseif( $percentage<50){
                $grade="E";
                $result="Fail";
              }

              $sel_Total=exeQuery("select * from ".TABLE_SUBJECT." where category_id='".$res_student['courses']."' ");

foreach ($sel_Total as $mark['total_marks'] =>$marks)
               {
                $Total3+=$marks['total_marks'];
                 $Total2+=$marks['pass_mark'];
              }

?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Page</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <style>
         table {
         border-collapse: collapse;
         border-spacing: 0;
         width: 100%;
         border: 1px solid #ddd;
         }
         th, td {
         text-align: left;
         padding: 8px;
         }
         tr:nth-child(even){background-color: #f2f2f2}
         .acedemy{
         text-align: center ;
         width: 339px;
         height: 77px;
         padding: 14px;
         }
         .acedemy-img{
         height:59px; 
         width:322px;
         }
      </style>
   </head>
   <body>
      <div class="container" style="border: 2px black solid;">
         <br>
         <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
               <div class="acedemy">
                  <div class="row">
                     <div class="acedemy-img">
                        <img src="http://thebsel.in/images/logobsel.jpeg">
                     </div>
                  </div>
                  <!-- <div class="row">
                     <div class="col"><b>COMPUTER TRANING ACADEMY</b></div>
                       </div> -->
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12" align="center">
               </br>
               <p>An ISO 9001-2015 Certified Institution</p>
               <h4><b>An ISO 9001-2015 Certified Institution</b></h4>
               <h3><?php echo $res_course['course_name'];?></h3>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <table class="table table-borderless responsive" align="center">
                  <thead>
                     <tr>
                        <td>Name :</td>
                        <td><?php echo $res_student['fullname'];?></td>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>Reg.Number</td>
                        <td><?php echo "BS"."-".$res_branch['branch_code']."-".$res_student['reno'];?></td>
                     </tr>
                     <tr>
                        <td>Branch Code</td>
                        <td><?php echo "BS"."-".$res_branch['branch_code'];?></td>
                     </tr>
                     <tr>
                        <td>Head Office</td>
                        <td>Bhagwanpur</td>
                     </tr>
                     <tr>
                        <td>Month Of Examination</td>
                        <td>April 2018</td>
                     </tr>
                    <!--  <tr>
                        <td>Certificate Number</td>
                        <td>3405</td>
                     </tr> -->
                  </tbody>
               </table>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12" style="overflow-x:auto;">
               <table class="table table-bordered responsive">
                  <thead>
                     <tr>
                        <th>Subjects</th>
                        <th>Total Marks</th>
                        <th>Pass Marks</th>
                        <th>Marks Secured</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php 
					 
                     while($res_subject=fetchAssoc($sel_subject))
                     {
						
						
                       $sel_obtained=exeQuery("select * from ".TABLE_RESULT." where subject_id='".$res_subject['id']."' ");

							$res_obtained=fetchAssoc($sel_obtained);
                     ?>
                     <tr>
                        <td><?php echo $res_subject['subject_name'];?></td>
                        <td><?php echo $res_subject['total_marks'];?></td>
                        <td><?php echo $res_subject['pass_mark'];?></td>
                        <td><?php echo $res_obtained['marks_obtained'];?></td>
                     </tr>
                     <?php
                      }
                     ?>
                     <tr>
                        <td>Total</td>
                        <td><?php echo $Total3;?></td>
                        <td><?php echo $Total2;?></td>
                        <td><?php echo $Total;?></td>
                     </tr>
                     <tr>
                        <td>result</td>
                        <td colspan="4" align="center"><?php echo $result;?></td>
                     </tr>
                     <tr>
                        <td>grade</td>
                        <td colspan="4" align="center"><?php echo $grade;?></td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <div class="col-md-4"><b>DATE:</b></div>
         </div>
         <br>
         <div class="row">
            <div class="col-md-6"><b>centre Head </b></div>
            <br>
            <div class="col-md-6"> <b>Director</b></div>
         </div>
         <br>
         <div class="row">
            <!-- <div class="col-md-12">
               <b>Topics Covered:</b>Computer Fundamentals,DOS,Windows XP,MS office 2007,MS-Access,Tally,and internet.
            </div> -->
            <br>
            <div class="col-md-12">
               <b>Grade Scale:</b>A+=>=80,A=> =70<80,B=>60&<=70,C=>50 &<60,D=>40&<50.
            </div>
         </div>
         <hr>
         <div class="row">
            <div class="col-md-12" align="center">
               <h5>Head Office</h5>
               <p>Savitri Complex BhagwanpurChattirewa Road Muzaffapur-842001</p>
               <p>Contact Number:9835429138,8678044493</p>
               <p>E-mail:thebsel@gmail.com,www.thebsel.org</p>
            </div>
         </div>
      </div>
   </body>
</html>