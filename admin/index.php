<?php
include("../library/raso_function.php");
check_admin();

s
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo SITENAME;?></title>

    <!-- BOOTSTRAP STYLES-->
    <link href="css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />


</head>
<?php
include("php/header.php");
?>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">DASHBOARD</h1>
                        <h2 style="text-align:center;"> Welcome to <strong> Thebsel</strong> </h2>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
				<?php 
                    if($_SESSION['type']=="branch")
                    {
                ?>
				  <div class="col-md-4">
                        <div class="main-box mb-pink">
                            <a href="student.php">
                                <i class="fa fa-users fa-5x"></i>
                                <h5>Students</h5>
                            </a>
                        </div>
                    </div>
                    <?php
                    }elseif($_SESSION['type']=="admin"){

                    ?>
                    <div class="col-md-4">
                        <div class="main-box mb-pink">
                            <a href="student.php">
                                <i class="fa fa-users fa-5x"></i>
                                <h5>Students</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="main-box mb-dull">
                            <a href="add-course.php">
                                <i class="fa fa-book fa-5x"></i>
                                <h5>Courses</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="main-box mb-dull">
                            <a href="branch.php">
                                <i class="fa fa-home fa-5x"></i>
                                <h5>Branch</h5>
                            </a>
                        </div>
                    </div>
                     <div class="col-md-4">
                        <div class="main-box mb-red">
                            <a href="add-manager.php">
                                <i class="fa fa-users fa-5x"></i>
                                <h5>Manager</h5>
                            </a>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                  

                </div>
                <!-- /. ROW  -->

            
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->

    <div id="footer-sec">
        Thebsel | Brought To You By : <a href="http://www.backstagesupporters.com" target="_blank">Backstagesupporters Pvt Ltd</a>
    </div>
   
   <script src="js/jquery-1.10.2.js"></script>	
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="js/custom1.js"></script>
    


</body>
</html>
