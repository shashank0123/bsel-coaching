<?php
include("../library/raso_function.php");
check_admin();
$errormsg = '';
$action = "add";

$sel_user=exeQuery("select * from admin where id='".$_SESSION['id']."' ");
$res_user=fetchAssoc($sel_user);

if(isset($_POST['save']))
{
	$cnt=$_POST['file_count'];
	
	$hid_id=($_POST['update']!='')?addslashes($_POST['update']):false;

	if($hid_id==false){

		for($f=1;$f<=$cnt;$f++)
		{

			$subject_id=$_POST['subject_id'.$f];
			$mark=$_POST['mark'.$f];

			echo "<br>";
			$date=date('Y-m-d');
			$student_id=$_POST['student_id'];

			$marks=exeQuery("insert into ".TABLE_RESULT." set student_id='".$student_id."',subject_id='".$subject_id."',marks_obtained='".$mark."',created_by='".$_SESSION['id']."',created_at='".$date."',status=1  "); 

		}

	}else{

		for($f=1;$f<=$cnt;$f++)
		{

			$subject_id=$_POST['subject_id'.$f];
			$mark=$_POST['mark'.$f];

			echo "<br>";
			$date=date('Y-m-d');
			$student_id=$_POST['student_id'];

			$marks=exeQuery("update ".TABLE_RESULT." set marks_obtained='".$mark."',created_by='".$_SESSION['id']."',created_at='".$date."',status=1 where student_id='".$student_id."' and subject_id='".$subject_id."' ");
		}
	}
	

}


if(isset($_GET['action']) && $_GET['action']=="delete"){

	$delete=exeQuery("delete from  student WHERE Id='".$_GET['id']."'");	
	header("location: marks.php?act=3");

}


$action = "add";
if(isset($_GET['action']) && $_GET['action']=="edit" ){
	$id = isset($_GET['id'])?addslashes($_GET['id']):'';

	$sqlEdit = exeQuery("SELECT * FROM result WHERE student_id='".$id."'");
	$res_edit=fetchAssoc($sqlEdit);
}


if(isset($_REQUEST['act']) && @$_REQUEST['act']=="1")
{
	$errormsg = "<div class='alert alert-success'> <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong> Student Add successfully</div>";
}else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="2")
{
	$errormsg = "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> <strong>Success!</strong> Student Edit successfully</div>";
}
else if(isset($_REQUEST['act']) && @$_REQUEST['act']=="3")
{
	$errormsg = "<div class='alert alert-success'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Success!</strong> Student Delete successfully</div>";
}

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php echo SITENAME;?></title>

	<!-- BOOTSTRAP STYLES-->
	<link href="css/bootstrap.css" rel="stylesheet" />
	<!-- FONTAWESOME STYLES-->
	<link href="css/font-awesome.css" rel="stylesheet" />
	<!--CUSTOM BASIC STYLES-->
	<link href="css/basic.css" rel="stylesheet" />
	<!--CUSTOM MAIN STYLES-->
	<link href="css/custom.css" rel="stylesheet" />
	<!-- GOOGLE FONTS-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

	<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
	
	<link href="css/ui.css" rel="stylesheet" />
	<link href="css/datepicker.css" rel="stylesheet" />	
	
	<script src="js/jquery-1.10.2.js"></script>
	
	<script type='text/javascript' src='js/jquery/jquery-ui-1.10.1.custom.min.js'></script>

	
</head>
<?php
include("php/header.php");
?>
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h1 class="page-head-line">Marks  
					<?php
					echo (isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")?
					' <a href="marks.php" class="btn btn-primary btn-sm pull-right">Back <i class="glyphicon glyphicon-arrow-right"></i></a>':'<a href="marks.php?action=add" class="btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-plus"></i> Add </a>';
					?>
				</h1>

				<?php

				echo $errormsg;
				?>
			</div>
		</div>



		<?php 
		if(isset($_GET['action']) && @$_GET['action']=="add" || @$_GET['action']=="edit")
		{
			?>

			<script type="text/javascript" src="js/validation/jquery.validate.min.js"></script>
			<div class="row">
				
				<div class="col-sm-10 col-sm-offset-1">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<?php echo ($action=="add")? "Add Marks": "Edit Marks"; ?>
						</div>
						<form action="marks.php" method="post" id="signupForm1" class="form-horizontal" enctype="multipart/form-data">
							<div class="panel-body">
								<fieldset class="scheduler-border" >
									<legend  class="scheduler-border">Personal Information:</legend>
									<?php
									$i=1;
									
									$select_student=exeQuery("select * from ".TABLE_STUDENT." where id='".$_GET['id']."' ");

									$res_student=fetchAssoc($select_student);

									$select_subject=exeQuery("select * from ".TABLE_SUBJECT." where category_id='".$res_student['courses']."'  ");
									$fetch_i = 0;
									while($res_subject=fetchAssoc($select_subject)){

										$sel_val=exeQuery("select * from result where subject_id='".$res_subject['id']."' and student_id='".$_GET['id']."' ");
										$res_val=fetchAssoc($sel_val);
										?>
										<div class="form-group">
											<label class="col-sm-2 control-label" for="Old"><?php
											echo $res_subject['subject_name'];

											?></label>
											<input type="hidden" name="subject_id<?php echo $i; ?>" value="<?php echo $res_subject['id'];?>">
											<div class="col-sm-4">
												<input type="text" class="form-control" id="marks" name="mark<?php echo $i; ?>" value="<?php echo $res_val['marks_obtained'];?>"  />
											</div>
										</div>
										<input type="hidden" name="file_count" id="file_count" value="<?php echo $i;?>">
										<input type="hidden" name="update" id="file_count" value="<?php echo $res_val['marks_obtained'];?>">
										<?php

										$i++;
									}
									?>
								</fieldset>
								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-2">
										<input type="hidden" name="id" value="<?php echo $id;?>">
										<input type="hidden" name="hid_id" value="<?php echo $res_edit['Id'];?>">
										<input type="hidden" name="student_id" value="<?php echo $res_student['Id'];?>">

										<button type="submit" name="save" class="btn btn-primary">Save </button>
									</div>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>
			<script type="text/javascript">
				$( document ).ready( function () {			

					$( "#joindate" ).datepicker({
						dateFormat:"yy-mm-dd",
						changeMonth: true,
						changeYear: true,
						yearRange: "1970:<?php echo date('Y');?>"
					});	

					if($("#signupForm1").length > 0)
					{

						<?php if($action=='add')
						{
							?>

							$( "#signupForm1" ).validate( {
								rules: {
									sname: "required",
									fname: "required",
									rollno: "required",
									regno: "required",
									dob: "required",
									password: "required",
									courses: "required",
									joindate: "required",
									emailid: "email",
									branch: "required",


									contact: {
										required: true,
										digits: true
									},

									fees: {
										required: true,
										digits: true
									},


									advancefees: {
										required: true,
										digits: true
									},


								},
								<?php
							}else
							{
								?>

								$( "#signupForm1" ).validate( {
									rules: {
										sname: "required",
										fname: "required",
										rollno: "number",
										regno: "required",
										password: "required",
										dob: "required",
										courses: "required",
										joindate: "required",
										emailid: "email",
										branch: "required",


										contact: {
											required: true,
											digits: true
										}

									},



									<?php
								}
								?>

								errorElement: "em",
								errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					// Add `has-feedback` class to the parent div.form-group
					// in order to add icons to inputs
					element.parents( ".col-sm-10" ).addClass( "has-feedback" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}

					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !element.next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-remove form-control-feedback'></span>" ).insertAfter( element );
					}
				},
				success: function ( label, element ) {
					// Add the span element, if doesn't exists, and apply the icon classes to it.
					if ( !$( element ).next( "span" )[ 0 ] ) {
						$( "<span class='glyphicon glyphicon-ok form-control-feedback'></span>" ).insertAfter( $( element ) );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-error" ).removeClass( "has-success" );
					$( element ).next( "span" ).addClass( "glyphicon-remove" ).removeClass( "glyphicon-ok" );
				},
				unhighlight: function ( element, errorClass, validClass ) {
					$( element ).parents( ".col-sm-10" ).addClass( "has-success" ).removeClass( "has-error" );
					$( element ).next( "span" ).addClass( "glyphicon-ok" ).removeClass( "glyphicon-remove" );
				}
			} );

							}

						} );



							$("#fees").keyup( function(){
								$("#advancefees").val("");
								$("#balance").val(0);
								var fee = $.trim($(this).val());
								if( fee!='' && !isNaN(fee))
								{
									$("#advancefees").removeAttr("readonly");
									$("#balance").val(fee);
									$('#advancefees').rules("add", {
										max: parseInt(fee)
									});

								}
								else{
									$("#advancefees").attr("readonly","readonly");
								}

							});




							$("#advancefees").keyup( function(){

								var advancefees = parseInt($.trim($(this).val()));
								var totalfee = parseInt($("#fees").val());
								if( advancefees!='' && !isNaN(advancefees) && advancefees<=totalfee)
								{
									var balance = totalfee-advancefees;
									$("#balance").val(balance);

								}
								else{
									$("#balance").val(totalfee);
								}

							});


						</script>
						<?php
					}else{
						?>
						<link href="css/datatable/datatable.css" rel="stylesheet" />
						<div class="panel panel-default">
							<div class="panel-heading">
								Manage Student  
							</div>
							<div class="panel-body">
								<div class="table-sorting table-responsive">
									<table class="table table-striped table-bordered table-hover" id="tSortable22">
										<thead>
											<tr>
												<th>Id
												</th>
												<th>Name/Contact</th>
												<th>Branch</th>
												<th>Course</th>
												<th>DOJ</th>
												<th>Fees</th>
												<th>Balance</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php

											if($_SESSION['type']=="branch")
											{
												$sql=exeQuery("select * from student where branch='".$res_user['branch']."' ");

											}else{
												$sql=exeQuery("select * from student");
											}
											$i=1;
											while($r = fetchAssoc($sql))
											{
												$sel_course=exeQuery("select * from ".TABLE_COURSES." where id='".$r['courses']."' ");
												$res_course=fetchAssoc($sel_course);

												$sele_branch=exeQuery("select * from ".TABLE_BRANCH." where id='".$r['branch']."' ");
												$res_branch=fetchAssoc($sele_branch);
												?>
												<td><?php echo $i;?></td>
												<td><?php echo $r['fullname'];?><br/><?php echo $r['mob'];?></td>
												<td><?php echo $res_branch['branch_name'];?></td>
												<td><?php echo $res_course['course_name'];?></td>
												<td><?php echo date("d M y", strtotime($r['joindate']));?></td>
												<td><?php echo $r['fees'];?></td>
												<td><?php echo $r['balance'];?></td>
												<td>
													<a href="marks.php?action=edit&id=<?php echo $r['Id'];?>" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></a>
												</td>
											</tr>
											<?php
											$i++;
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<script src="js/dataTable/jquery.dataTables.min.js"></script>

					<script>
						$(document).ready(function () {
							$('#tSortable22').dataTable({
								"bPaginate": true,
								"bLengthChange": true,
								"bFilter": true,
								"bInfo": false,
								"bAutoWidth": true });

						});


					</script>

					<?php
				}
				?>



			</div>
			<!-- /. PAGE INNER  -->
		</div>
		<!-- /. PAGE WRAPPER  -->
	</div>
	<!-- /. WRAPPER  -->

	<div id="footer-sec">
		Thebsel | Brought To You By : <a href="http://www.backstagesupporters.com" target="_blank">Backstagesupporters Pvt Ltd</a>
	</div>


	<!-- BOOTSTRAP SCRIPTS -->
	<script src="js/bootstrap.js"></script>
	<!-- METISMENU SCRIPTS -->
	<script src="js/jquery.metisMenu.js"></script>
	<!-- CUSTOM SCRIPTS -->
	<script src="js/custom1.js"></script>
	<script type="text/javascript">
		function callval(){
			var course_id = $('#course_id').val();
			$.ajax({
				url:"load_price.php",
				type:"POST",
				data:{course_id:course_id},
				success:function(data){
					$('#fees').val(data);
				}

			});
		}

		function generate(id){
			$.ajax({
				url:"generate_certificate.php",
				type:"POST",
				data:{id:id},
				success:function(data){
					alert(data);
				}

			});
		}

		function result(id){
			$.ajax({
				url:"generate_result.php",
				type:"POST",
				data:{id:id},
				success:function(data){
					alert(data);
				}

			});
		}
	</script>


</body>
</html>
