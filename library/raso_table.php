<?php
// define DATABASE and all table used in software
date_default_timezone_set("Asia/Kolkata");
error_reporting(0);
define("TABLE_LOGIN","login");
define("TABLE_BRANCH","branch");
define("TABLE_COURSES","courses");
define("TABLE_NOTICE","notice");
define("TABLE_HOMEWORK","homework");
define("TABLE_STUDENT","student");
define("TABLE_CERTIFICATE","certificate");
define("TABLE_SUBJECT","subject");
define("TABLE_RESULT","result");


define("SITENAME","Thebesel");
define("I_MAIL","jha17678@gmail.com");
define("E_MAIL","jha17678@gmail.com");

define("LOCALHOST","localhost");
define("USERNAME","root");
define("PASSWORD","");
define("DATABASE","bsel");
//define("DATABASE","srms2");



define("ERROR404","error-404.php");

define("INSERT_MSG","Record have been successfully inserted");
define("UPDATE_MSG","Record have been successfully updated");
define("DELETE_MSG","Record have been successfully deleted");

define("INSERT_ERROR","Record have not been inserted");
define("UPDATE_ERROR","Record have not been updated");
define("DELETE_ERROR","Record have not been deleted");
define("ERROR_MSG","Fail to execute...");

?>