-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2019 at 05:16 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bsel`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `mob` varchar(255) NOT NULL,
  `emailid` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `updationDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `UserName`, `Password`, `mob`, `emailid`, `branch`, `type`, `updationDate`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '', '', 'admin', '2019-07-22 13:50:43'),
(3, 'shivam', 'd41d8cd98f00b204e9800998ecf8427e', '9560381698', 'shivamjha0912@gmail.com', '9', 'branch', '2019-07-23 11:05:44'),
(4, 'rekha', '3a857fef7005ff7ba49aac2e6e5ad1e6', '8130170947', 'rekha@gmail.com', '10', 'branch', '2019-07-18 09:14:20'),
(5, 'arun', 'e10adc3949ba59abbe56e057f20f883e', '4343434434', 'arunthakur05@gmail.com', '11', 'branch', '2019-07-23 09:24:01'),
(6, 'kumar', '', '123456', 'arunthakur0522@gmail.com', '11', 'branch', '2019-07-23 09:12:14'),
(9, 'rwefdf', 'd41d8cd98f00b204e9800998ecf8427e', '4343434434', 'arunthakur053333@gmail.com', '11', 'branch', '2019-07-23 11:11:54');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `branch_code` varchar(5) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `created_by` varchar(15) NOT NULL,
  `created_at` date NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `branch_name`, `branch_code`, `mobile`, `email`, `address`, `details`, `created_by`, `created_at`, `status`) VALUES
(9, 'sonia vihar', 'soni', '9560381698', 'soniavihar@gmail.com', 'delhi', 'sonia vihar', '1', '2019-07-20', 1),
(10, 'yamuna vihar', 'yamu', '9560381698', 'yamunavihar@gmail.com', 'yamuna vihar', 'india', '1', '2019-07-20', 1),
(11, 'Arun', 'arun', '9868961002', 'arunthakur05@gmail.com', 'delhi', 'rerrerrrrr', '1', '2019-07-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `certificate`
--

CREATE TABLE `certificate` (
  `id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `reg_no` varchar(255) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `grade` varchar(15) NOT NULL,
  `date` date NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certificate`
--

INSERT INTO `certificate` (`id`, `student_id`, `student_name`, `parent_name`, `reg_no`, `course_name`, `grade`, `date`, `created_by`, `status`) VALUES
(3, '', 'arun11', 'kumar11', '1', 'C / C++', 'D', '2019-07-23', '', 1),
(4, '6', 'shivam', 'shivam', '4', 'ADCA', 'E', '2019-07-23', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `fees` varchar(255) NOT NULL,
  `course_desc` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `starting_date` date NOT NULL,
  `duration` varchar(50) NOT NULL,
  `seats` varchar(50) NOT NULL,
  `created_by` varchar(10) NOT NULL,
  `created_at` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `subject_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_name`, `fees`, `course_desc`, `image`, `starting_date`, `duration`, `seats`, `created_by`, `created_at`, `status`, `subject_id`) VALUES
(3, 'DCA', '1800', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et123', '1563426476f1.jpg', '2018-12-31', '3 months', '3012', '1', '2019-07-23', 1, '6,8'),
(4, 'ADCA', '1500', 'Advanced Diploma in Computer Application (CCA + DCA + CFA + DTP = ADCA)', '1563426329ab10.jpg', '2019-08-02', '3 months', '30', '1', '2019-07-23', 1, '6,7,8'),
(5, 'C / C++', '5000', 'The objective of the course is to enable a student to acquire the knowledge pertaining to C /C++`', '1563515668ab14.jpg', '2019-12-30', '3 months', '30', '1', '2019-07-23', 1, '6,7,8');

-- --------------------------------------------------------

--
-- Table structure for table `fees_transaction`
--

CREATE TABLE `fees_transaction` (
  `id` int(11) NOT NULL,
  `stdid` varchar(255) NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `fees` varchar(255) NOT NULL,
  `balance` varchar(255) NOT NULL,
  `paid` int(255) NOT NULL,
  `submitdate` datetime NOT NULL,
  `transcation_remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fees_transaction`
--

INSERT INTO `fees_transaction` (`id`, `stdid`, `course_id`, `fees`, `balance`, `paid`, `submitdate`, `transcation_remark`) VALUES
(1, '', '4', '1500', '500', 1000, '2019-07-20 00:00:00', '2019-07-20'),
(2, '1', '4', '1500', '1000', 500, '0000-00-00 00:00:00', 'all clear'),
(3, '', '4', '1500', '800', 700, '2019-07-20 00:00:00', '800 rs pending'),
(4, '2', '4', '1500', '1000', 500, '2019-07-20 00:00:00', '300 rs pending'),
(5, '', '4', '1500', '200', 1300, '2019-07-20 00:00:00', ''),
(6, '3', '4', '1500', '1400', 100, '2019-07-20 00:00:00', '100 rs pending'),
(7, '6', '5', '5000', '5000', 0, '2019-07-23 00:00:00', ''),
(8, '8', '5', '5000', '0', 5000, '2019-07-23 00:00:00', 'tytrytrty');

-- --------------------------------------------------------

--
-- Table structure for table `homework`
--

CREATE TABLE `homework` (
  `id` int(11) NOT NULL,
  `course_id` varchar(55) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `doc` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `homework`
--

INSERT INTO `homework` (`id`, `course_id`, `branch_id`, `doc`, `created_at`, `created_by`, `status`) VALUES
(3, '3', 0, '156345032618090472664-Phase-II_AdmitCard.pdf', '2019-07-18', '1', 1),
(5, '4', 0, '156388261815634501001539845476Presentation2-converted.pdf', '2019-07-23', '5', 1),
(7, '5', 0, '156388266715634501001539845476Presentation2-converted.pdf', '2019-07-23', '5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(11) NOT NULL,
  `notice` text NOT NULL,
  `created_at` date NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  `marks_obtained` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `result`
--

INSERT INTO `result` (`id`, `student_id`, `subject_id`, `marks_obtained`, `created_by`, `created_at`, `status`) VALUES
(1, '1', '6', '100', '1', '2019-07-22', 1),
(2, '1', '7', '100', '1', '2019-07-22', 1),
(3, '2', '6', '80', '1', '2019-07-20', 1),
(4, '2', '7', '85', '1', '2019-07-20', 1),
(5, '8', '10', '50', '1', '2019-07-23', 1),
(6, '8', '11', '50', '1', '2019-07-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `Id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `mob` varchar(255) NOT NULL,
  `reno` varchar(255) NOT NULL,
  `emailid` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `courses` varchar(255) NOT NULL,
  `father` varchar(255) NOT NULL,
  `rollno` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `photo2` varchar(255) NOT NULL,
  `idcard` varchar(255) NOT NULL,
  `marksheet` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `dob` datetime NOT NULL,
  `pass` varchar(255) NOT NULL,
  `joindate` datetime NOT NULL,
  `fees` int(255) NOT NULL,
  `balance` int(255) NOT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`Id`, `fullname`, `mob`, `reno`, `emailid`, `password`, `courses`, `father`, `rollno`, `image`, `photo2`, `idcard`, `marksheet`, `gender`, `branch`, `dob`, `pass`, `joindate`, `fees`, `balance`, `delete_status`) VALUES
(1, 'SHIVAM', '9560381698', '1', 'shivamjha0912@gmail.com', '123456', '4', 'dinesh jha', '', '1563613338t1.jpg', '', '', '', '', '9', '1995-12-09 00:00:00', '123456', '2019-07-20 00:00:00', 1500, 0, '0'),
(2, 'Rekha', '9560381698', '2', 'abc@gmail.com', '123456', '4', 'nhi pta', '', '1563613682bb3.jpg', '', '', '', '', '9', '1995-04-24 00:00:00', '123456', '2019-07-20 00:00:00', 1500, 300, '0'),
(3, 'Navin', '9560381698', '1', 'navin@gmail.com', '123456', '4', 'navin', '', '1563613927t2.jpg', '', '', '', '', '10', '1995-12-09 00:00:00', '123456', '2019-07-20 00:00:00', 1500, 100, '0'),
(4, 'tarun', '956038198', '3', 'shivam@gmail.com', '123456', '4', 'tarun', '', '1563619328t1.jpg', '', '', '', '', '9', '1995-12-01 00:00:00', 's=shivam', '2019-07-20 00:00:00', 1500, 1000, '0'),
(6, 'shivam', '9898765432', '4', 'arunthakur05@gmail.com', '123456', '4', 'shivam', '', '1563622908t2.jpg', '', '', '', '', '9', '2019-07-24 00:00:00', '123456', '2019-07-20 00:00:00', 1500, 300, '0'),
(8, 'arun11', '3333333333', '1', 'arunthakur0533@gmail.com', '123456', '5', 'kumar11', '', '1563874665Desert.jpg', '', '', '', '', '11', '2019-07-09 00:00:00', '123456', '2019-07-03 00:00:00', 5000, 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(11) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `total_marks` varchar(255) NOT NULL,
  `pass_mark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`id`, `subject_name`, `created_by`, `created_at`, `status`, `category_id`, `total_marks`, `pass_mark`) VALUES
(6, 'ms-office', '1', '2019-07-19', 1, '4', '100', '40'),
(7, 'ms-word', '1', '2019-07-19', 1, '4', '100', '40'),
(8, 'ms excel', '1', '2019-07-19', 1, '3', '100', '40'),
(10, 'C++', '1', '2019-07-23', 1, '5', '100', '40'),
(11, 'C', '1', '2019-07-23', 1, '5', '100', '40');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `emailid` varchar(255) NOT NULL,
  `lastlogin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Id`, `username`, `password`, `name`, `emailid`, `lastlogin`) VALUES
(1, 'admin', '3a857fef7005ff7ba49aac2e6e5ad1e6', 'Lewa', 'lewa@gmail.com', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificate`
--
ALTER TABLE `certificate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fees_transaction`
--
ALTER TABLE `fees_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homework`
--
ALTER TABLE `homework`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `certificate`
--
ALTER TABLE `certificate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fees_transaction`
--
ALTER TABLE `fees_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `homework`
--
ALTER TABLE `homework`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
